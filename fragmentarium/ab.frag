#include "Progressive2D.frag"

vec3 color(vec2 ab) {
	float rB = ab.x + 3.0;
	float rA = ab.y + 3.0;
	if (0.0 < rA && rA < 4.0 && 0.0 < rB && rB < 4.0) {
		float l = 0.0;
		float n = 0.0;
		float z = 0.5;
		float dz;
		for (int i = 0; i < 128; ++i) {
			z = rA * z * (1.0 - z);
			z = rB * z * (1.0 - z);
		}
		for (int i = 0; i < 1024; ++i) {
			z = rA * z * (1.0 - z);
			dz = abs(rA * (1.0 - 2.0 * z));
			if (dz > 0.0) {
				dz = log(dz);
				if (-1000.0 < dz && dz < 1000.0) {
					l += dz;
					n += 1.0;
				}
			}
			z = rB * z * (1.0 - z);
			dz = abs(rB * (1.0 - 2.0 * z));
			if (dz > 0.0) {
				dz = log(dz);
				if (-1000.0 < dz && dz < 1000.0) {
					l += dz;
					n += 1.0;
				}
			}
		}
		if (n > 0.0) {
			l /= n;
			if (l <= 0.01) {
				return vec3(1.0 + tanh(l / 4.0), 1.0 + tanh(l / 3.0), 0.0);
			} else {
				return vec3(0.0, 0.0, tanh(l * 3.0));
			}
		} else {
			return vec3(1.0, 1.0, 0.0);
		}
	} else {
		return vec3(0.0);
	}
}
