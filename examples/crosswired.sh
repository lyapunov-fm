#!/bin/bash
mkdir -p crosswired
w=16
h=12
( cat <<-EOF
	lyapunov-fm/0
	geometry = 1024x768
	tiling   = 1x1+0+0
	delay    = 0.75
	radius   = 0.5
	note_x   = 111.879204
	note_y   = 124.428627
	index_x  = 45.643826
	index_y  = 1.264403
	matrix   = [ 1, 1, 0, 0 ; 0, 0, 1, 1 ]
	render(crosswired/crosswired_preview.pgm)
	geometry = 600x600
EOF
for y in $(seq 0 $((h - 1)))
do
  for x in $(seq 0 $((w - 1)))
  do
    cat <<-EOF
	tiling   = ${w}x${h}+${x}+${y}
	render(crosswired/tile_${y}_${x}.pgm)
EOF
  done
done ) | ../src/lyapunov-fm 0.75 --batch
for y in $(seq 0 $((h - 1)))
do
  pnmcat -lr $(for x in $(seq 0 $((w - 1))) ; do echo "crosswired/tile_${y}_${x}.pgm" ; done) > "crosswired/row_${y}.pgm"
done
pnmcat -tb $(for y in $(seq $((h - 1)) -1 0) ; do echo "crosswired/row_${y}.pgm" ; done) > "crosswired/crosswired.pgm"
