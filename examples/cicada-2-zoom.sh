#!/bin/bash
set -e
s="cicada--2-zoom"
w=32
h=18
mkdir -p "${s}"
( cat <<-EOF
	lyapunov-fm/0
	delay    = 0.750000
	note_x   = 93.291428
	note_y   = 106.493973
	index_x  = 5.687781
	index_y  = -38.408806
	matrix   = [ 1, -1, 0, 0 ; 0, 0, 1, -1 ]
	geometry = 240x240
EOF
for r in 0.25 0.5 1 2 4 8 16 32 64
do
  mkdir -p "${s}/${r}"
  echo "radius = ${r}"
  for y in $(seq $((h - 1)) -1 0)
  do
    for x in $(seq 0 $((w - 1)))
    do
      cat <<-EOF
	tiling = ${w}x${h}+${x}+${y}
	render(${s}/${r}/tile_${y}_${x}.pgm)
EOF
    done
  done
done ) | ../src/lyapunov-fm 0.75 --batch
for r in 0.25 0.5 1 2 4 8 16 32 64
do
  for y in $(seq 0 $((h - 1)))
  do
    pnmcat -lr $(for x in $(seq 0 $((w - 1))) ; do echo "${s}/${r}/tile_${y}_${x}.pgm" ; done) > "${s}/${r}/row_${y}.pgm"
  done
  pnmcat -tb $(for y in $(seq $((h - 1)) -1 0) ; do echo "${s}/${r}/row_${y}.pgm" ; done) > "${s}/${r}.pgm"
  pnmtopng -interlace -compression 9 -force "${s}/${r}.pgm" > "${s}/${r}.png"
  convert "${s}/${r}.png" -normalize -geometry "1920x1080" -quality 90 "${s}/${r}.jpg"
done
