#!/bin/bash
set -e
s="cicada"
w=16
h=9
mkdir -p "${s}"
( cat <<-EOF
	lyapunov-fm/0
	geometry = 1280x720
	tiling   = 1x1+0+0
	delay    = 0.750000
	radius   = 0.262415
	note_x   = 93.291428
	note_y   = 106.493973
	index_x  = 5.687781
	index_y  = -38.408806
	matrix   = [ 1.000000, 1.000000, 0.000000, 0.000000 ; 0.000000, 0.000000, 1.000000, 1.000000 ]
	render(${s}/small.pgm)
	geometry = 256x256
EOF
for y in $(seq $((h - 1)) -1 0)
do
  for x in $(seq 0 $((w - 1)))
  do
    cat <<-EOF
	tiling   = ${w}x${h}+${x}+${y}
	render(${s}/tile_${y}_${x}.pgm)
EOF
  done
done ) | ../src/lyapunov-fm 0.75 --batch
for y in $(seq 0 $((h - 1)))
do
  pnmcat -lr $(for x in $(seq 0 $((w - 1))) ; do echo "${s}/tile_${y}_${x}.pgm" ; done) > "${s}/row_${y}.pgm"
done
pnmcat -tb $(for y in $(seq $((h - 1)) -1 0) ; do echo "${s}/row_${y}.pgm" ; done) > "${s}/${s}.pgm"
pnmtopng -interlace -compression 9 -force "${s}/${s}.pgm" > "${s}/${s}.png"
convert "${s}/${s}.png" -normalize -geometry "1920x1080" -quality 90 "${s}/${s}.jpg"
