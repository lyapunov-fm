#!/bin/bash
set -e
s="delay"
mkdir -p "${s}"
for d in $(seq 0 27)
do
  cat <<-EOF | ../src/lyapunov-fm ${d} --batch
	lyapunov-fm/0
	geometry = 1280x720
	tiling   = 1x1+0+0
	delay    = ${d}
	radius   = 48
	note_x   = 96
	note_y   = 96
	index_x  = 0
	index_y  = 0
	matrix   = [ 1, -1, 0, 0 ; 0, 0, 1, -1 ]
	render(${s}/${d}.pgm)
EOF
  pnmtopng -interlace -compression 9 -force "${s}/${d}.pgm" > "${s}/${d}.png"
done
