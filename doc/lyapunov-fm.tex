\documentclass[11pt,a4paper]{article}
\usepackage{lac2013}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
\usepackage{dblfloatfix}
\usepackage{subfigure}
%\usepackage{hyperref}
\newcommand{\url}[1]{\texttt{#1}}
\sloppy
\newenvironment{contentsmall}{\small}

\DeclareMathOperator{\inc}{I}
\DeclareMathOperator{\wrap}{\%}
\DeclareMathOperator{\SR}{SR}

\title{Lyapunov Space of Coupled FM Oscillators}
\author
{Claude Heiland-Allen
\\ claude@mathr.co.uk
}

\begin{document}
\maketitle

\begin{abstract}
\begin{contentsmall}
Consider two coupled oscillators, each modulating the other's
frequency.  This system is governed by four parameters: the base
frequency and modulation index for each oscillator.  For some
parameter values the system becomes unstable.  The Lyapunov
exponent is used to measure the instability.  Images of the
parameter space are generated, with the number crunching implemented on graphics
hardware using OpenGL.  The mouse position over the displayed
image is linked to realtime audio output, creating an audio-visual browser
for the 4D parameter space.
\end{contentsmall}
\end{abstract}

\keywords{
\begin{contentsmall}
chaos, DSP, GPU
\end{contentsmall}
}

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{example.png}
\caption{Example output.}
\label{example}
\end{figure}

\section{Introduction}
Soft Rock EP~\cite{SoftRockEP} and Soft Rock DVD~\cite{SoftRockDVD}
explored the transitions between order and chaos in coupled FM
oscillators.  A more recent continuation of
this project is to make a map of the parameter space of coupled FM oscillators on a perceptually
relevant level and use it in live performance, choosing parameters on the basis
of desired sound character.

A bifurcation diagram produced by an analogue Moog
synthesizer~\cite{Slater98} and images of Lyapunov fractals~\cite{Dewdney91}
were inspiration to apply the latter technique to the parameter space
of coupled FM oscillators in the digital realm.

\newpage

\section{Formulation}

\subsection{Coupled FM Oscillators}
\begin{figure}[H]
\centering
\includegraphics[width=0.4\textwidth]{figure1.png}
\caption{Coupled FM oscillators in Pure-data.}
\label{fig:pd}
\end{figure}
Consider the two coupled oscillators in Figure~\ref{fig:pd}.
Pure-data's model of interconnected components each with their
own internal state maps poorly to GPU architecture.  Considering
the whole system as one and flattening the internal state into
a single phase space vector leads to the following formulation
as a mutual recurrence relation:
\begin{equation}\begin{aligned}
x_{n+1} &= \wrap(x_{n} + \inc(f_x + m_x \cos (2 \pi y_{n-d}))) \\
y_{n+1} &= \wrap(y_{n} + \inc(f_y + m_y \cos (2 \pi x_{n-d})))
\end{aligned}\label{eq:osc}\end{equation}
where
\begin{equation*}\wrap(t) = t - \lfloor t \rfloor ,\quad \inc(t) = \frac{440}{\SR} 2^{\frac{t - 69}{12}}\end{equation*}
Here $x_n$, $y_n$ is the phase of each oscillator at time step $n$,
$d$ is a delay measured in samples, $f_x$, $f_y$ is the base frequency
of each oscillator as a MIDI note number, and $m_x$, $m_y$ is the
modulation index of each oscillator as a MIDI note number.  $\wrap(t)$
performs wrapping into $[0,1)$, with $\lfloor t \rfloor$ being the
flooring operation (the largest integer not greater than $t$).

The four-dimensional parameter space vector will be written
\begin{equation*}a=(f_x,f_y,m_x,m_y)\end{equation*}
and the $(2d+2)$-dimensional phase space vector
\begin{equation*}z=(x_n,y_n,x_{n-1},y_{n-1},\ldots,x_{n-d},y_{n-d})\end{equation*}
with sample rate $\SR = 48000$.  For reasons
explained in Section~\ref{sec:glissues}, $d=1$ will be fixed.

\subsection{Lyapunov Exponents}

Lyapunov exponents can be used to measure the stability (or otherwise)
of a dynamical system.
A good introduction is found in Chapter~4.3 \emph{Lyapunov Exponent}~\cite{Elert07}.
The definition is covered in Chapter~13.7 \emph{Liapounov exponents and entropies}~\cite{Falconer03}
which also relates it to measures of fractal dimension.

The Lyapunov exponent $\lambda$ measures divergence in phase space:
\begin{equation*}\left| z_1(t) - z_0(t) \right| \approx e^{\lambda t} \left| z_1(0) - z_0(0) \right|\end{equation*}
\begin{equation}\lambda = \lim_{\begin{array}{c} t \to \infty \\
z_1(0) \to z_0(0)\end{array}} \frac{1}{t} \log{\frac{\left|z_1(t) - z_0(t)\right|}{\left|z_1(0) - z_0(0)\right|}}\label{eq:lexp}\end{equation}
An attracting orbit has $\lambda < 0$ and a divergent (chaotic) orbit
has $\lambda > 0$.

A modified norm is required to take into account
the wrapping of phase into $[0,1)$:
\begin{equation*}\left|z\right|_{\wrap} = \sqrt{\sum_{i} {\left(\min(\wrap(z_i), 1 - \wrap(z_i))\right)^2}}\end{equation*}
For example the distance between $0.1$ and $0.9$ is properly $0.2$ (not $0.8$)
because $0.1$ can be phase-unwrapped to $1.1$.

\subsection{Viewing Planes}

An image is 2D, which requires choosing a subset of the 4D parameter
space to visualize.  Two particular planes were chosen:
\begin{equation}\begin{aligned}
A_+(a_0, r_0) &= a_0 + r_0 \left(\begin{array}{rr}
1 & 0 \\
\phantom{-}1 & 0 \\
0 & 1 \\
0 & \phantom{-}1 \end{array}\right)\left(\begin{array}{c}u \\
v \end{array}\right) \\
A_-(a_0, r_0) &= a_0 + r_0 \left(\begin{array}{rr}
 1 &  0 \\
-1 &  0 \\
 0 &  1 \\
 0 & -1 \end{array}\right)\left(\begin{array}{c}u \\
v \end{array}\right)\end{aligned}\label{eq:view}\end{equation}
where $(u,v)$ is the coordinates of the pixel, $a_0$ is the
centre of the view, and $r_0$ is the radius of the view.

These planes were chosen because they are simple, while still being
flexible enough to explore the whole 4D space.
The $A_+$ plane varies both oscillators in the same direction, while
the $A_-$ plane varies each oscillator in opposite directions.  To
center on a particular target point $(f_x, f_y, m_x, m_y)$ one might
use the $A_+$ plane to center on the midpoint
\begin{equation*}\left(\frac{f_x + f_y}{2}, \frac{f_x + f_y}{2}, \frac{m_x + m_y}{2}, \frac{m_x + m_y}{2}\right)\end{equation*}
and then switch to the $A_-$ plane to break the $(x,y)$ symmetry.

\section{Implementation}

The implementation uses OpenGL~\cite{OpenGL} and OpenGL Shading Language~\cite{GLSL}
for computation and graphical rendering, GLUT~\cite{GLUT} for windowing
and input event handling, and JACK~\cite{JACK} for audio output.

\subsection{Introduction to Modern OpenGL}

Modern OpenGL has a programmable shader pipeline.  Vertex attributes are read
from vertex buffers and processed by vertex shaders.  The outputs
of the vertex shader (called varyings) are further manipulated by an
optional geometry shader stage.  Geometry shaders can output a different
vertex count to their input count, whereas vertex shaders are one-in
one-out.  The result of the geometry shader can be captured into another
vertex buffer using transform feedback.  Following the geometry
shader the primitives (points or triangles) are rasterized, and
varyings interpolated across each primitive.  Finally a fragment
shader takes these values and computes the colour at that pixel.  The
output of a fragment shader can be captured by attaching a texture to
a framebuffer.

\subsection{Computation Overview}

To render an image a texture is first filled with $(u,v)$ coordinates
using a framebuffer object and a fragment shader.  This texture is copied
to a vertex buffer object, interleaved with an initial phase space vector
$z=(0,0,0,0)$ and Lyapunov exponent statistics vector $l=(0,0,0,0)$ for
each point.

Using a vertex shader, $a$ is calculated from $(u,v)$ using Equation~\ref{eq:view}, and then
a rough estimate of the Lyapunov exponent is computed using
Equation~\ref{eq:lexp} by perturbing $z_1(0) = z_0(0) + \delta$ with
$\delta$ small and performing $t = 256$ iterations of Equation~\ref{eq:osc}.
The first few repetitions are discarded, along with those resulting in $-\infty$,
and the rough $\lambda$ estimates are accumulated in $l$.

Between each repetition the working set is compacted using a geometry
shader.  Points whose mean Lyapunov exponent estimate changed
very little during the previous step are plotted and removed from the
working set.  The other points are kept to be refined
further, directing the computational effort on the points that
need it most:~those slow to converge.

To ensure user interface responsiveness, the computation is amortized
over several frames.  The target frame period is divided by the measured
time for one repetition to compute how many repetitions to perform that
frame.  The repetitions-per-frame increases as the working set becomes
smaller.

\subsection{Noise Increases Stability}

At the end of each repetition $z_1$ is kept instead of $z_0$.  This
effectively adds a small amount of noise, counter-intuitively
increasing stability.  Noise allows more of the phase space to be
explored, and makes it more likely for the perturbed orbit to reach an
attracting part of the phase space.

\subsection{Dither Increases Quality}

To reduce grid sampling artifacts, $(u,v)$ is perturbed within the bounds
of its corresponding pixel before calculating the $a$ parameter vector
for each repetition.

\section{Results}

\begin{figure*}[ht!]
\centering
\begin{tabular}{cc}
\subfigure[$A_+((120, 120, 0, 0), 72)$]{\includegraphics[width=3in]{example01.png}\label{ex:01}} &
\subfigure[$A_-((120, 120, 0, 0), 72)$]{\includegraphics[width=3in]{example02.png}\label{ex:02}} \\
\subfigure[$A_+((95.2, 95.2, 32.6, 32.6), 4.5)$]{\includegraphics[width=3in]{example03.png}\label{ex:03}} &
\subfigure[$A_-((95.2, 95.2, 32.6, 32.6), 4.5)$]{\includegraphics[width=3in]{example04.png}\label{ex:04}} \\
\subfigure[$A_+((151.57, 151.57, 1.64, 1.64), 0.07)$]{\includegraphics[width=3in]{example05.png}\label{ex:05}} &
\subfigure[$A_-((151.57, 151.57, 1.64, 1.64), 0.07)$]{\includegraphics[width=3in]{example06.png}\label{ex:06}} \\
\subfigure[$A_-((117.0, 148.4, 20.4, 2.7), 1.8)$]{\includegraphics[width=3in]{example07.png}\label{ex:07}} &
\subfigure[$A_+((103.65, 108.41, 33.42, 10.93), 0.14)$]{\includegraphics[width=3in]{example09.png}\label{ex:09}}
\end{tabular}
\caption{Example images.  Darker shades are stable, lighter shades chaotic.}
\label{fig:examples1}
\end{figure*}
%\begin{figure}[ht]
%\centering
%\subfigure[$A_-((117.0, 148.4, 20.4, 2.7), 1.8)$]{\includegraphics[width=3in]{example07.png}\label{ex:07}}
%\subfigure[$A_-((141.46, 146.22, 22.76, 0.27), 0.14)$]{\includegraphics[width=3in]{example08.png}\label{ex:08}}
%\subfigure[$A_+((103.65, 108.41, 33.42, 10.93), 0.14)$]{\includegraphics[width=3in]{example09.png}\label{ex:09}}
%\subfigure[$A_-((89.8, 137.5, -17.5, -7.1), 3.7)$]{\includegraphics[width=3in]{example10.png}\label{ex:10}}
%\caption{More examples.}
%\label{fig:examples2}
%\end{figure}

\subsection{Examples}

Figure~\ref{ex:01} shows the initial view on starting the interactive
browser.  Low frequencies to the left are stable even at high
modulation index away from the central axis.  High frequencies to the
right become chaotic at progressively lower modulation index.
\subref{ex:02} shows the $A_-$ plane at the same location.
\subref{ex:03} shows bands alternating between stability and
chaos.  The bands become distorted and collapse as the modulation
index and frequency increase.  \subref{ex:04} shows its $A_-$
plane, bands become rings.
When the frequency is greatly increased, the shapes become more
intricate.  \subref{ex:05} exhibits spirals of stability, with
similar spirals in the $A_-$ plane in \subref{ex:06}.

When $f_x=f_y$ and $m_x=m_y$ the $A_+$ plane has mirror symmetry about its
horizontal axis, and the $A_-$ plane has two-fold rotational symmetry
about its centre.
Breaking the symmetry and setting $f_x \ne f_y$ or $m_x \ne m_y$ leads
to diverse forms.  In particular Figure~\ref{ex:09} has shapes that
resemble those of Lyapunov space images of the logistic map.

\subsection{Interactive Explorer}

The implementation is an interactive audio-visual explorer for the parameter
space of coupled FM oscillators.
Clicking with the mouse zooms the view about the clicked point.
The left button (or scroll up) zooms in, the right button (or scroll
down) zooms out, the middle button centers the view on the target point.
Pressing the TAB key toggles between the $A_+$ and $A_-$ planes in
Equation~\ref{eq:view}, and F11 toggles full screen operation.

While the GPU simulates and analyses one oscillator pair per pixel, the
CPU simulates one oscillator pair with $a$ determined from the pixel
under the mouse pointer.  The image acts as a map,
a reference frame for chosing parameters to audition by moving the mouse.

\section{Conclusions}

\subsection{Original Intent}

Earlier experiments used one Pure-data batch mode instance per CPU
core each sending analysis data to a realtime Pure-data instance.
The analysis used various methods (including FFT for spectral
statistics and the sigmund external for pitch tracking) to classify
points into pitched (ordered, stable) or unpitched (chaotic, unstable)
with measures of distortion or noisiness.  Sadly this approach proved
impractical as it achieved only tens of pixels per second, even
with a fast multi-core CPU, and porting these signal analysis algorithms
to massively-parallel programmable graphics hardware seemed to be too
difficult.

\subsection{OpenGL Issues \label{sec:glissues}}

The current implementation is hardcoded with delay $d=1$ and would be
very awkward to generalize.  OpenGL architecture limits each vertex
attribute to four components with the maximum number of attributes
typically limited to sixteen.  This totals 64 floats per vertex, 6 of
which are needed for the pixel coordinates and Lyapunov exponent
statistics accumulation.  Therefore using OpenGL imposes a limit
$d < 28$.  For comparison the original experiments in
\emph{Soft Rock EP} used Pure-data's default block size of 64, with $d = 32$.
Moreover, increasing $d$ increases video memory consumption.  With
the maximum $d = 27$, browsing at $1920 \times 1080$ resolution would
require over 1GB.

Future work on this project will look into using OpenCL, which
provides a heterogenous CPU and GPU computation framework, in the hope that
it will avoid the inherent awkwardness of abusing OpenGL shaders to
perform calculations.

\subsection{Audio Issues}

While the implementation works as intended, with $d=1$
the sound is nowhere near as rich and varied as with $d=32$.  With
small $d$ there is much more very high frequency content in
interesting-looking regions.  There seem to be few if any regions of the
parameter space with both interesting appearance and palatable audio
frequencies at $d=1$, while with high $d$ there are
parameters that generate sounds that fluctuate intermittently between
smooth tones and noise.  Visualization with high $d$ has not been
possible so far, so whether their neighbourhoods
look as interesting as they sound remains an openquestion .

Unfortunately, heavy use of the GPU in the interactive browser can
block the operating system for too long and cause audible glitches
(JACK xruns).  This situation may change as free drivers continue to
improve, allowing use of the browser in a live situation.

\subsection{Pretty Pictures}

Despite these shortcomings, I think the images look good.
I plan to render a selection at high resolution and print
postcards and posters.  For huge images it is possible to divide the image plane into tiles and
compute each tile in succession, finally combining the pieces
into one large picture.

There is also scope for video work, moving and rotating the viewing
plane through the 4D parameter space, with different shapes forming
and collapsing over time.  Rough benchmarks take 5-10 seconds
per frame at $1920 \times 1080$, so it seems sensible to wait until faster cheaper
graphics cards become available.

\section{Obtaining the Implementation}

The implementation was written on GNU/Linux Debian Wheezy running on a quad-core AMD64 processor
with NVIDIA GTX~550Ti graphics card using
proprietary drivers.  The source code is available at:
\url{http://code.mathr.co.uk/lyapunov-fm}


\section{Acknowledgements}

Thanks to the anonymous reviewers for their constructive criticism
on a number of issues, and to Rob Canning, Adnan Hadzi, and
Joanne Seale for their helpful feedback on earlier versions of this
paper.

\bibliographystyle{acl}
\bibliography{lyapunov-fm}

\end{document}
