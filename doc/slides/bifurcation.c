#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define W 1920
#define H 1080
#define N 50000
#define A 16

int main(int argc, char **argv) {
  int *m = calloc(1, W * H * sizeof(int));
  unsigned char *grey = calloc(1, W * H);
  unsigned char *rgb = calloc(1, W * H * 3);
  for (int x = 0; x < W; ++x) {
    float l = 0;
    int n = 0;
    for (int dx = 0; dx < A; ++ dx) {
      float r = 2 + (x + dx / (float) A) * 2 / W;
      float z = 0.5;
      for (int i = 0; i < 500; ++i) {
        z = r * z * (1 - z);
      }
      for (int i = 0; i < N; ++i) {
        z = r * z * (1 - z);
        float dz = fabsf(r * (1 - 2 * z));
        if (dz > 0) {
          l += logf(dz);
          n += 1;
        }
        float dither = rand() / (float) RAND_MAX - 0.5f;
        int y = fminf(fmaxf(H * (1 - z) + dither, 0), H - 1);
        m[y * W + x]++;
      }
    }
    if (n != 0) {
      l /= n;
    } else {
      l = -100;
    }
    float v = 0.5 + 0.5 * tanh(3.0 * l);
    int cr = fminf(fmaxf((255 * 0.6 * (1.0 - tanh(4.0 * (v - 0.3333))) - 30) * 255.0 / 225, 0), 255);
    int cg = fminf(fmaxf((255 * (2.0 * v * v - 2.5 * v + 1) - 30) * 255.0 / 225, 0), 255);
    int cb = fminf(fmaxf((255 * v * v - 30) * 255.0 / 225, 0), 255);
    for (int y = 0; y < H; ++ y) {
      float ca = log2f(1 + m[y * W + x]) / log2f(1 + A * N);
      grey[y * W + x] = 255 * (1 - ca);
      rgb[3 * (y * W + x) + 0] = 255 * ca + (1 - ca) * cr;
      rgb[3 * (y * W + x) + 1] = 255 * ca + (1 - ca) * cg;
      rgb[3 * (y * W + x) + 2] = 255 * ca + (1 - ca) * cb;
    }
  }
  printf("P5\n%d %d\n255\n", W, H);
  fwrite(grey, W * H, 1, stdout);
  printf("P6\n%d %d\n255\n", W, H);
  fwrite(rgb, W * H * 3, 1, stdout);
  return 0;
}
