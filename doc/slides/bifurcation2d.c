#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define W (1080*4)
#define H (1080*4)
#define N 2500
#define A 1

int main(int argc, char **argv) {
  float lmin =  1000;
  float lmax = -1000;
  printf("P6\n%d %d\n255\n", W, H);
  for (int y = H - 1; y >= 0; --y) {
    for (int x = 0; x < W; ++x) {
      float l = 0;
      int n = 0;
      for (int dx = 0; dx < A; ++ dx) {
        for (int dy = 0; dy < A; ++ dy) {
          float rB = 2 + (x + dx / (float) A) * 2 / W;
          float rA = 2 + (y + dy / (float) A) * 2 / H;
          float z = 0.5;
          for (int i = 0; i < 250; ++i) {
            z = rA * z * (1 - z);
            z = rB * z * (1 - z);
          }
          for (int i = 0; i < N; ++i) {
            z = rA * z * (1 - z);
            float dz = fabsf(rA * (1 - 2 * z));
            if (dz > 0) {
              l += logf(dz);
              n += 1;
            }
            z = rB * z * (1 - z);
            dz = fabsf(rB * (1 - 2 * z));
            if (dz > 0) {
              l += logf(dz);
              n += 1;
            }
          }
        }
      }
      float v = -100;
      if (n != 0) {
        l /= n;
        v = l;
        lmin = fminf(lmin, l);
        lmax = fmaxf(lmax, l);
      }
      float fr, fg, fb;
      if (v <= 0) {
        fr = (1 + tanhf(v / 4)) * 255;
        fg = (1 + tanhf(v / 3)) * 255;
        fb = 0;
      } else {
        fr = 0;
        fg = 0;
        fb = tanhf(3 * v) * 255;
      }
/*
      int cr = fminf(fmaxf((255 * 0.6 * (1.0 - tanhf(4.0 * (v - 0.3333))) - 64) * 255.0 / 191, 0), 255);
      int cg = fminf(fmaxf((255 * (2.0 * v * v - 2.5 * v + 1) - 64) * 255.0 / 191, 0), 255);
      int cb = fminf(fmaxf((255 * v * v - 64) * 255.0 / 191, 0), 255);
*/
#define O(c) putchar(fminf(fmaxf(c, 0), 255))
      O(fr);O(fg);O(fb);
    }
  }
  fprintf(stderr, "%f %f\n", lmin, lmax);
  return 0;
}
