{-# LANGUAGE NoMonomorphismRestriction #-}
module Main (main) where

import Diagrams.Prelude
import Diagrams.Backend.Cairo.CmdLine (defaultMain)

import Control.Monad (forM_)
import Data.List (intersperse)
import Data.Maybe (fromJust)
import System.Environment (withArgs)

main = do
  forM_ (diagrams slides `zip` [100..]) $ \(d, i) ->
    withArgs ["-o", "o/" ++ tail (show i ++ ".png"), "-h", "1080"] $
      defaultMain d

diagrams = map style . presentation

slides =
  ( "Lyapunov FM"
  ,[( "Background"
    ,[( "Recurrence Relations"
      ,[( "Recurrence"
        , [ "Next state depends on current state."
          , "z(t+1) = F(z(t))"
      ]),("Phase Space"
        , [ "Internal state that varies over time."
          , "z(0) determines all future z(t)."
      ]),("Parameter Space"
        , [ "Constant over time."
          , "Family of recurrences."
      ]),("Example: Logistic Map"
        , [ "z(t+1) = r z(t) (1 - z(t))"
          , "1D phase space: z(t)"
          , "1D parameter space: r"
      ])]
    ),( "Behaviour Of Recurrences"
      ,[( "Orbits"
        , [ "The set of points reached from z(0)."
          , "{ z(0), z(1), z(2), ... }"
      ]),("Attractors"
        , [ "Orbits may become periodic."
          , "Many points can reach one attractor."
      ]),("Stability"
        , [ "Nearby points are pulled closer together."
      ]),("Chaos"
        , [ "Nearby points are pushed further apart."
          , "Butterfly effect."
      ]),("Lyapunov Exponent"
        , [ "Quantifies butterfly effect."
          , "< 0 : stable"
          , "> 0 : chaotic"
      ])]
    ),( "Mapping Behaviours"
      ,[( "1D Logistic Map"
        , [ "z(t+1) = F(z(t))"
          , "F(x) = r x (1 - x)"
          , "1D phase space: z(t)"
          , "1D parameter space: r"
      ]),("1D Bifurcation Diagram"
        , [ "#LogisticBifurcation"
      ]),("1D Lyapunov Space"
        , [ "#LogisticLyapunov1D"
      ]),("2D Logistic Map"
        , [ "z(t+1) = G(F(z(t)))"
          , "F(x) = A x (1 - x)"
          , "G(x) = B x (1 - x)"
          , "1D phase space: z(t)"
          , "2D parameter space: (A,B)"
      ]),("2D Lyapunov Space"
        , [ "#LogisticLyapunov2D"
      ])])
  ]),("Implementation"
    ,[( "Coupled FM Oscillators"
      ,[( "Pure-data Implementation"
        , [ "#Pure-data"
      ]),("Recurrence Relation"
        , [ "x(t+1) = F(fx, mx, x(t), y(t-d))"
          , "y(t+1) = F(fy, my, y(t), x(t-d))"
          , "F(f,m,u,v) = wrap(u + I(f + m cos(2 pi v)))"
          , "I(n) = 440/SR 2^((n - 69)/12) "
      ]),("4D Parameter Space"
        , [ "Base frequency (oscillator X)"
          , "Base frequency (oscillator Y)"
          , "Modulation index (oscillator X)"
          , "Modulation index (oscillator Y)"
      ])]
    ),( "GPU Overview"
      ,[( "OpenGL Pipeline"
        , [ "#OpenGLPipeline"
      ]),("OpenGL Glossary"
        , [ "shader: stage in the pipeline"
          , "program: a linked set of shaders"
          , "uniform: parameters for a program"
          , "attribute: per-vertex shader input"
          , "varying: data passed between shaders"
          , "texture: 2D array stored in GPU memory"
          , "buffer: 1D array stored in GPU memory"
      ]),("Hardware Limits"
        , [ "16 attributes"
          , "4 components per attribute"
          , "2(d+1) components for phase space vector"
          , "5 components needed elsewhere"
          , "d <= 28 with optimal packing"
      ]),("Buffer Memory Layout"
        , [ "#Memory"
      ]),("Vertex Attribute Pointers"
        , [ "#Pointers"
      ])]
    ),( "GPU Details"
      ,[( "Computation Structure"
        , [ "#Compute"
      ]),( "Fill RGBA Texture with uv00"
        , [ "#Fill"
      ]),("Copy RG Channels to Buffer"
        , [ "#Copy"
      ]),("Initialize State"
        , [ "#Init"
--      ]),("Step: Unpack State"
--        , [ "#Unpack"
      ]),("Step Recurrence"
        , [ "#Step"
--      ]),("Step: Pack State"
--        , [ "#Pack"
      ]),("Prune Working Set"
        , [ "Memory bandwidth limits speed"
          , "Some points converge faster"
          , "Geometry shader culls converged points"
          , "Working set becomes leaner"
      ]),("Plot Points"
        , [ "Render to framebuffer texture"
          , "gl_FragColor = raw L statistics"
      ]),("Colour Image"
        , [ "Read raw L statistics from texture"
          , "gl_FragColor = F(l)"
          , "stable → yellow"
          , "chaotic → blue"
      ])]
    ),( "Lyapunov Exponents"
      ,[( "Informal Definition"
        , [ "|Δz(t)| = exp(L t) |Δz(0)|"
          , "L < 0 stable; nearby values pulled closer"
          , "L > 0 chaotic; nearby values pushed apart"
      ]),("Formal Definition"
        , [ "L = 1/t log |Δz(t)|/|Δz(0)|"
          , "Take limits as t→infinity and Δz(0)→0"
      ]),("Calculation in 1D"
        , [ "Inner limit become derivative"
          , "Recurrence derivative is product of step derivatives"
          , "Log of product is sum of logs"
      ]),("Calculation in >1D"
        , [ "Inner limit becomes Jacobian"
          , "Outer limit defines a matrix"
          , "Log of matrix eigenvalues are Lyapunov spectrum"
          , "Maximal Lyapunov exponent measures stability"
      ]),("Numerical Calculation"
        , [ "Pick a point on the attractor z(0)"
          , "Pick a nearby point w(0)"
          , "Run recurrence on both until t large"
          , "Compute L = 1/t log |z(t)-w(t)|/|z(0)-w(0)|"
          , "Repeat, averaging all the results"
      ])])
  ]),("Conclusions"
    ,[( "Examples"
      ,[( "d = 0"
        , [ "#Example0"
      ]),("d = 1"
        , [ "#Example1"
      ]),("d = 4"
        , [ "#Example4"
      ]),("d = 16"
        , [ "#Example16"
      ])]
    ),( "Evaluation"
      ,[( "Good Points"
        , [ "Operation at 0 <= d <= 27"
          , "Experimental fractional d (interpolation)"
          , "Batch mode for offline rendering"
          , "Tiled rendering of huge images"
      ]),("Bad Points"
        , [ "JACK xruns with heavy GPU load"
          , "Free drivers might be better than proprietary?"
          , "Perceptual (ir)relevance of Lyapunov exponent"
          , "Tiled rendering random seed glitches"
          , "Long video render times even with fast hardware"
      ])]
    ),( "Ends"
      ,[( "Links"
        , [ "http://mathr.co.uk/lyapunov-fm"
          , "claude@mathr.co.uk"
  ])])])])

images =
  [("LogisticBifurcation", d'bifur)
  ,("LogisticLyapunov1D", d'bifurc)
  ,("LogisticLyapunov2D", d'bifurc2)
  ,("Pure-data", d'pd)
  ,("OpenGLPipeline", d'opengl)
  ,("Compute", d'compute)
  ,("Memory", d'memory)
  ,("Pointers", d'pointers)
  ,("Fill", d'fill)
  ,("Copy", d'copy)
  ,("Init", d'init)
  ,("Unpack", d'unpack)
  ,("Step", d'step)
  ,("Pack", d'pack)
  ,("Example0", hcat [image "0_pos.png" 9 9, strutX 1, image "0_neg.png" 9 9] # centerXY)
  ,("Example1", hcat [image "1_pos.png" 9 9, strutX 1, image "1_neg.png" 9 9] # centerXY)
  ,("Example4", hcat [image "4_pos.png" 9 9, strutX 1, image "4_neg.png" 9 9] # centerXY)
  ,("Example16", hcat [image "16_pos.png" 9 9, strutX 1, image "16_neg.png" 9 9] # centerXY)
  ]

presentation (t, ss) = ([], mempty) : ([t], bullets (map fst ss)) : concatMap (section [t]) ss

section ts (t, ss) = (t:ts, bullets (map fst ss)) : concatMap (subsection (t:ts)) ss

subsection ts (t, ss) = (t:ts, bullets (map fst ss)) : concatMap (slide (t:ts)) ss

slide ts (t, ss) = [(t:ts, bullets ss)]

bullet ('#':im) = fromJust $ im `lookup` images
bullet s = llabel s (0, 0) # scale 1.5 # translate (r2(-11.5, 0))

bullets ss = (vcat . intersperse (llabel "" (0,0)) . map bullet) ss # alignT # translate (r2(0, 3))

style (ts, d)
  = (d <> (frame ts # centerXY))
  # font "LMSans10"
  # lw 0.05
  # lc black
  # lineCap LineCapRound
  # lineJoin LineJoinRound
  # fc white
  # centerXY
-- # pad 1.1
  # bg white

frame [] = mconcat
  [ alignedText 0 0.5 "Lyapunov Space of" # scale 1.5 # translate (r2(0.5, 14)) # fc black # bold
  , alignedText 0 0.5 "Coupled FM Oscillators" # scale 1.5 # translate (r2(0.5, 12.5)) # fc black # bold
  , alignedText 0 0.5 "Claude Heiland-Allen" # scale 1.5 # translate (r2(0.5, 10)) # fc black
  , alignedText 0 0.5 "Linux Audio Conference 2013" # scale 1.5 # translate (r2(0.5, 7.5)) # fc black
  , box (0, 0) (24, 18)
  ]
frame (title:ts) = mconcat $
  [ alignedText 0 0.5 t # scale 0.5 # translate (r2(0.5, 17.25 - i))# fc black | (t, i) <- reverse ts `zip` [0..] ] ++
  [ alignedText 0 0.5 title # scale 1.5 # translate (r2(0.5, 14)) # fc black # bold
  , alignedText 0 0.5 "Claude Heiland-Allen" # scale 0.5 # translate (r2(0.5, 0.75)) # fc black
  , alignedText 1 0.5 "Linux Audio Conference 2013" # scale 0.5 # translate (r2(23.5, 0.75)) # fc black
  , box (0, 0) (24, 18)
  ]

label s p = bound (text s # scale 0.75) # translate (r2 p) # fc black

llabel s p = bound (alignedText 0 0.5 s # scale 0.75) # translate (r2 p) # fc black

rlabel s p = bound (alignedText 1 0.5 s # scale 0.75) # translate (r2 p) # fc black

bound d = d `atop` phantom' (square 0.5)

phantom' d = phantom d `asTypeOf` d

withEnvelope' d d' = withEnvelope (d `asTypeOf` d') d'

line p q = fromVertices [p2 p, p2 q]

triangle p q r = fromVertices [p2 p, p2 q, p2 r, p2 p] # fc black

box (x0, y0) (x1, y1) = mconcat
  [ line (x0, y0) (x1, y0)
  , line (x1, y0) (x1, y1)
  , line (x1, y1) (x0, y1)
  , line (x0, y1) (x0, y0)
  ]

xarrow (x0, y0) (x1, y1) = mconcat
  [ line (x0, y0) (x1 - 0.25, y1)
  , triangle (x1, y1) (x1 - 0.5, y1 + 0.25) (x1 - 0.5, y1 - 0.25)
  ]

yarrow (x0, y0) (x1, y1) = mconcat
  [ line (x0, y0) (x1, y1 - 0.25)
  , triangle (x1, y1) (x1 - 0.25, y1 - 0.5) (x1 + 0.25, y1 - 0.5)
  ]

nxarrow (x0, y0) (x1, y1) = mconcat
  [ line (x0, y0) (x1 + 0.25, y1)
  , triangle (x1, y1) (x1 + 0.5, y1 + 0.25) (x1 + 0.5, y1 - 0.25)
  ]

nyarrow (x0, y0) (x1, y1) = mconcat
  [ line (x0, y0) (x1, y1 + 0.25)
  , triangle (x1, y1) (x1 - 0.25, y1 + 0.5) (x1 + 0.25, y1 + 0.5)
  ]

d'fill = mconcat
  [ label "u" (9, 3.5)
  , label "v" (1.5, 8.5)
  , label "R" (17.5, 11.5)
  , label "G" (18.5, 11.5)
  , label "B" (19.5, 11.5)
  , label "A" (20.5, 11.5)
  , label "u" (17.5, 10.5), box (17, 10) (18, 11)
  , label "v" (18.5, 10.5), box (18, 10) (19, 11)
  , label "0" (19.5, 10.5), box (19, 10) (20, 11)
  , label "0" (20.5, 10.5), box (20, 10) (21, 11)
  , yarrow (2, 5) (2, 12)
  , xarrow (3, 4) (15, 4)
  , box (3, 5) (15, 12)
  , box (8, 7) (8.5, 7.5)
  , line (8, 7.5) (17, 11)
  , line (8.5, 7) (21, 10)
  ] # centerXY

d'copy = grid
  [ [ cell "11", cell "21", hdots, cell "w1" ]
  , [ cell "12", cell "22", hdots, cell "w2" ]
  , [ vdots', vdots', ddots, vdots' ]
  , [ cell "1h", cell "2h", hdots, cell "wh" ]
  ] # centerXY
  === strutY 1 === nyarrow (0, 2) (0, 0) === strutY 1 ===
  hcat [cell' "11", cell' "21", hdots, cell' "w1", cell' "12", hdots, cell' "w2", cell' "13", hdots, cell' "wh" ] # centerXY
  where vdots' = centerXY vdots <> phantom' (centerXY (box (0,0) (4,2)))

hdots = mconcat [ dot (0.5, 0.5), dot (1, 0.5), dot (1.5, 0.5), phantom' (box (0, 0) (2, 1)) ]

vdots = mconcat [ dot (0.5, 0.5), dot (0.5, 1), dot (0.5, 1.5), phantom' (box (0, 0) (1, 2)) ]

ddots = mconcat [ dot (0.5, 1.5), dot (1, 1), dot (1.5, 0.5), phantom' (box (0, 0) (2, 2)) ]

dot p = circle 0.1 # fc black # translate (r2 p)

cell [x,y] = hcat [sub ['u',x,y], sub ['v',x,y], sub "0", sub "0"]

cell' [x,y] = hcat [sub ['u',x,y], sub ['v',x,y]]

chunk _ [] = []
chunk n xs = let (ys, zs) = splitAt n xs in ys : chunk n zs

grid = vcat . map (centerXY . hcat . map centerXY)

d'init = ((vcat
  [ label "in" (6, 0.5)
  , strutY 7 # translate (r2(6,0))
  , vec2 "q" "u" "v"
  ] # centerXY) ||| strutX 1 ||| xarrow (0,0) (2, 0) ||| strutX 1 ||| (vcat
  [ label "out" (6, 0.5)
  , strutY 1 # translate (r2(6,0))
  , vec4' "p0" "0" "0" "0" "0"
  , vec4' "p1" "0" "0" "0" "0"
  , vdots # translate (r2(7.5, 0))
  , vec4' "pN" "0" "0" "0" "0"
  , vec4 "l" "0" "0" "0" "0"
  , vec2 "q" "u" "v"
  ] # centerXY)) # centerXY

d'unpack = ((vcat
  [ label "in" (6, 0.5)
  , strutY 2.5 # translate (r2(6,0))
  , vec4' "p0" "x0" "y0" "x1" "y1"
  , strutY 0.5 # translate (r2(6,0))
  , vdots # translate (r2(7.5, 0))
  , strutY 0.5 # translate (r2(6,0))
  , vec4' "pN" "xd-1" "yd-1" "xd" "yd"
  , strutY 0.5 # translate (r2(6,0))
  ] # centerXY) ||| strutX 1 ||| xarrow (0,0) (2, 0) ||| strutX 1 ||| (vcat
  [ label "out" (6, 0.5)
  , strutY 1 # translate (r2(6,0))
  , float "j" "0"
  , vec2A "p[D]" "x0" "y0"
  , vec2A'       "x1" "y1"
  , vdots # translate (r2(7.5, 0))
  , vec2A'       "xd-1" "yd-1"
  , vec2A'       "xd" "yd"
  ] # centerXY) # centerXY === strutY 2 === label "" (0, 0.5)) # centerXY

d'pack = ((vcat
  [ label "in" (6, 0.5)
  , strutY 1 # translate (r2(6,0))
  , float "j" "j"
  , vec2A "p[D]" "x0" "y0"
  , vec2A'       "x1" "y1"
  , vdots # translate (r2(7.5, 0))
  , vec2A'       "xd-1" "yd-1"
  , vec2A'       "xd" "yd"
  ] # centerXY) ||| strutX 1 ||| xarrow (0,0) (2, 0) ||| strutX 1 ||| (vcat
  [ label "out" (6, 0.5)
  , strutY 2.5 # translate (r2(6,0))
  , vec4' "p0" "xj" "yj" "xj+1" "yj+1"
  , strutY 0.5 # translate (r2(6,0))
  , vdots # translate (r2(7.5, 0))
  , strutY 0.5 # translate (r2(6,0))
  , vec4' "pN" "xj-2" "yj-2" "xj-1" "yj-1"
  , strutY 0.5 # translate (r2(6,0))
  ] # centerXY)) # centerXY === strutY 2 === 
  label "subscripts are wrapped into [0,D)" (0,0.5)

memrowlayout = memrowvec

memrowvec = hcat $
  map subV4 ["p0", "p1"] ++ [hdots, subV4 "pN", subV4 "l", subV2 "q"]

memrowodd = hcat $
  map sub ["x0", "y0", "x1", "y1", "x2", "y2", "x3", "y3"] ++
  [hdots] ++ map sub ["xd-1", "yd-1", "xd", "yd"] ++
  map ub ["1", "L", "LL", "", "u", "v"]

memroweven = hcat $
  map sub ["x0", "y0", "x1", "y1", "x2", "y2", "x3", "y3"] ++
  [hdots] ++ map sub ["xd", "yd", "  ", "  "] ++
  map ub ["1", "L", "LL", "", "u", "v"]

subV4 v = label' v (2, 0.5) <> box (0,0) (4,1)

subV2 v = label' v (1, 0.5) <> box (0,0) (2,1)

sub v = label' v (0.5, 0.5) <> box (0,0) (1,1)

ub v = label v (0.5, 0.5) <> box (0,0) (1,1)

float v x = mconcat
  [ label "float" (4, 0.5), phantom' (box (3, 0) (5, 1))
  , label v (6, 0.5)
  , label x (7.5, 0.5), box (7, 0) (8, 1)
  ]

vec2 v x y = mconcat
  [ label "vec2" (4, 0.5), phantom' (box (3, 0) (5, 1))
  , label v (5.5, 0.5)
  , label x (6.5, 0.5), box (6, 0) (7, 1)
  , label y (7.5, 0.5), box (7, 0) (8, 1)
  ]

vec2A v x y = mconcat
  [ label "vec2" (4, 0.5), phantom' (box (3, 0) (5, 1))
  , label v (6, 0.5)
  , label' x (7.5, 0.5), box (7, 0) (8, 1)
  , label' y (8.5, 0.5), box (8, 0) (9, 1)
  ]

vec2A' x y = mconcat
  [ phantom' (box (3, 0) (5, 1))
  , label' x (7.5, 0.5), box (7, 0) (8, 1)
  , label' y (8.5, 0.5), box (8, 0) (9, 1)
  ]

vec4 v x y z w = mconcat
  [ label "vec4" (4, 0.5), phantom' (box (3, 0) (5, 1))
  , label v (5.5, 0.5)
  , label x (6.5, 0.5), box (6, 0) (7, 1)
  , label y (7.5, 0.5), box (7, 0) (8, 1)
  , label z (8.5, 0.5), box (8, 0) (9, 1)
  , label w (9.5, 0.5), box (9, 0) (10, 1)
  ]

label' [s1] (x, y) = label [s1] (x, y)
label' (s1:s2) (x, y) = mconcat [label [s1] (x - 0.1875, y), label s2 (2 * x + 0.5, 2 * y - 0.5) # scale 0.5]

label'' [s1] (x, y) = label [s1] (x, y)
label'' (s1:s2) (x, y) = mconcat [label [s1] (x, y), llabel s2 (2 * x + 0.5, 2 * y - 0.5) # scale 0.5]

vec4' v x y z w = mconcat
  [ label "vec4" (4, 0.5), phantom' (box (3, 0) (5, 1))
  , label' v (5.5, 0.5)
  , label' x (6.5, 0.5), box (6, 0) (7, 1)
  , label' y (7.5, 0.5), box (7, 0) (8, 1)
  , label' z (8.5, 0.5), box (8, 0) (9, 1)
  , label' w (9.5, 0.5), box (9, 0) (10, 1)
  ]

d'memory = vcat
  [ memrowvec # centerXY
  , strutY 1
  , text "d odd" # scale 0.75 # fc black <> strutY 1
  , strutY 0.5
  , memrowodd # centerXY
  , strutY 1
  , text "d even" # scale 0.75 # fc black <> strutY 1
  , strutY 0.5
  , memroweven # centerXY
  ]

d'pointers = vcat
  [ label "stride" (12, 0.5) <> phantom' (box (0,0) (1,1))
  , xarrow (2, 0.5) (22, 0.5) <> (withEnvelope' (box (0,0) (1,1)) $ (line (2, 0.5) (2, 0) <> line (22, 0.5) (22, 0)) # dashed)
  , memrowlayout # translate (r2(2, 0))
  , pointer "p0" 2 1
  , pointer "p1" 6 2
  , vdots <> phantom' (box (0,0) (1,1))
  , pointer "pN" 12 5
  , pointer "l"  16 6
  , pointer "q"  20 7
  ] # centerXY

pointer l x y = withEnvelope' p (p <> q)
  where
    p = label' l (0.5, 0.5) <> xarrow (1, 0.5) (x, 0.5) <> phantom' (box (0,0) (1,1))
    q = line (x, 0.5) (x, y) # dashed

d'step = vcat
  [ hcat [hdots, v "j-d-1", v "j-d", v "j-d+1", hdots, v "j-1", v "j", v "j+1", v "j+2", hdots]
  , mconcat
      [ line (5, 2) (5, 1)
      , line (13, 2) (13, 1)
      , line (5, 1) (13, 1)
      , line (9, 1) (9, 0)
      , line (9, 0) (15, 0)
      , yarrow (15, 0) (15, 2)
      ]
  ] # centerXY === strutY 1 ===
  image "recurrence-equation.png" 16 2
  === strutY 0.5 ===
  image "inc-equation.png" 8 2

v' [a,b] s = label'' ([a]++s) (0.5, 1) <> label'' ([b]++s) (0.5, 0) <> phantom' (box (0,-0.5) (2,1.5))

v s = v' "xy" s <> box (0,-0.5) (2,1.5)

d'pd = image "puredata.png" 9 9

d'opengl = mconcat
  [ label "vertex attributes" (-1, 1)
  , nyarrow (0, 0) (0, -2), llabel "vertex shader" (1, -1)
  , nyarrow (0, -3) (0, -5), llabel "geometry shader" (1, -4)
  , line (-0.5, -5.5) (-2, -5.5), yarrow (-2, -5.5) (-2, 0), rlabel "transform feedback" (-3, -2)
  , nyarrow (0, -6) (0, -8), llabel "fragment shader" (1, -7)
  , label "fragment colour" (-1, -9)
  ]

d'compute = mconcat
  [ label "fill" (1, 6)
  , label "copy" (4, 6)
  , label "init" (7, 6)
  , label "colour" (4, 1)
  , label "plot" (7, 1)
  , label "step" (7, 3.5)
  , label "prune" (13, 3.5)
  , xarrow (0, 5) (2, 5)
  , xarrow (3, 5) (5, 5)
  , xarrow (6, 5) (8, 5)
  , nxarrow (8, 2) (6, 2)
  , nxarrow (5, 2) (3, 2)
  , nyarrow (8.5, 4.5) (8.5, 2.5)
  , line (9, 2) (11, 2)
  , line (11, 2) (11, 5)
  , nxarrow (11, 5) (9, 5)
  , phantom' (box (-1,-0.5) (14.5, 7))
  ] # centerXY

d'bifur =
  image "bifurcation-grey.png" 16 9 # centerXY
  === strutY 0.5 ===
  (label "2" (0, 0.5) <> label "r" (8, 0.5) <> label "4" (16, 0.5)) # centerXY

d'bifurc =
  image "bifurcation-colour.png" 16 9 # centerXY
  === strutY 0.5 ===
  (label "2" (0, 0.5) <> label "r" (8, 0.5) <> label "4" (16, 0.5)) # centerXY

d'bifurc2 =
  (((label "4" (0.5, 9) <> label "B" (0.5, 4.5) <> label "2" (0.5, 0)) # centerXY)
   ||| strutX 0.5 ||| image "bifurcation-2d.png" 9 9 # centerXY) # centerXY
  === strutY 0.25 ===
  (phantom' (label " " (0.5, 0.5) # centerXY)
   ||| strutX 0.5 |||
   (label "2" (0, 0.5) <> label "A" (4.5, 0.5) <> label "4" (9, 0.5)) # centerXY
  ) # centerXY

dashed = dashing [0.125, 0.125] 0.1875
