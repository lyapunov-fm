#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  int count = 4096 * 4096;
  unsigned short *pgm = malloc(count * 2);
  FILE *f = fopen(argv[1], "rb");
  fseek(f, -count * 2, SEEK_END);
  fread(pgm, count * 2, 1, f);
  fclose(f);
  unsigned short mi = 65535, ma = 0;
  for (int i = 0; i < count; ++i) {
    if (mi > pgm[i]) mi = pgm[i];
    if (ma < pgm[i]) ma = pgm[i];
  }
  unsigned char *ppm = malloc(count * 3);
  int k = 0;
  for (int i = 0; i < count; ++i) {
    float g = pgm[i];
    if (g > 65536/2) {
      g -= 65536/2;
      g /= (ma - 65536/2);
      g *= 0.5;
      g += 0.5;
    } else {
      g -= mi;
      g /= (65536/2 - mi);
      g *= 0.5;
    }
    float x = 0.6 * (1.0 - tanh(4.0 * (g - 1.0/3.0)));
    float y = 2.0 * g * g - 2.5 * g + 1.0;
    float z = g * g;
    x -= 0.25; x /= 0.75; x = fmin(fmax(x, 0), 1); x = pow(x, 0.25);
    y -= 0.25, y /= 0.75; y = fmin(fmax(y, 0), 1); y = pow(y, 0.25);
    z -= 0.25, z /= 0.75; z = fmin(fmax(z, 0), 1); z = pow(z, 0.25);
    ppm[k++] = 255 * x;
    ppm[k++] = 255 * y;
    ppm[k++] = 255 * z;
  }
  f = fopen(argv[2], "wb");
  fprintf(f, "P6\n4096 4096\n255\n");
  fwrite(ppm, count * 3, 1, f);
  fclose(f);
  return 0;
}
