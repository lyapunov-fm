uniform mat4 mvp;
in vec4 q0;
smooth out vec2 t;
void main() {
  t = q0.zw;
  gl_Position = mvp * vec4(q0.xy, 0.0, 1.0);
}
