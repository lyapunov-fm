#ifndef VIEW_H
#define VIEW_H 1

struct view {
  int width;
  int height;
  glm::mat4 matrix;
  glm::vec4 center;
  float radius;
  int tilex, tiley, tilesx, tilesy;
  float delay;
};

void view_init(struct view *v, int width, int height, double centerx, double centery, double centerz, double centerw, double radius, double delay);
void view_copy(struct view *dst, struct view *src);
void view_reshape(struct view *v, int width, int height);
void view_reset(struct view *v, double centerx, double centery, double centerz, double centerw, double radius);
void view_plane(struct view *v, int which);
void view_coord(glm::vec4 *c, struct view *v, int px, int py);
void view_center(struct view *v, glm::vec4 &c);
void view_zoom(struct view *v, float z);
void view_zoom(struct view *v, glm::vec4 &c, float z);

#endif
