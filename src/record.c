#include "view.h"

struct record {
  unsigned char *buffer;
  int bytes;
  int seqno;
  bool screenshot;
  bool video;
  char *filename;
};

void record_begin(struct record *s) {
  s->buffer = 0;
  s->bytes = 0;
  s->seqno = 0;
  s->screenshot = false;
  s->video = false;
  s->filename = 0;
}

void record_end(struct record *s) {
  if (s->buffer) {
    free(s->buffer);
    s->buffer = 0;
    s->bytes = 0;
  }
}

void record_write(FILE *out, unsigned char *buffer, struct view *v) {
  fprintf(out, "P5\n");
  fprintf(out, "# lyapunov-fm/0\n");
  fprintf(out, "# geometry = %dx%d\n", v->width, v->height);
  fprintf(out, "# tiling   = %dx%d+%d+%d\n", v->tilesx, v->tilesy, v->tilex, v->tiley);
  fprintf(out, "# delay    = %f\n", v->delay);
  fprintf(out, "# radius   = %f\n", v->radius);
  fprintf(out, "# note_x   = %f\n", v->center[0]);
  fprintf(out, "# note_y   = %f\n", v->center[1]);
  fprintf(out, "# index_x  = %f\n", v->center[2]);
  fprintf(out, "# index_y  = %f\n", v->center[3]);
  fprintf(out, "# matrix   = [ %f, %f, %f, %f ; %f, %f, %f, %f ]\n"
    , v->matrix[0][0], v->matrix[0][1], v->matrix[0][2], v->matrix[0][3]
    , v->matrix[1][0], v->matrix[1][1], v->matrix[1][2], v->matrix[1][3]
    );
  fprintf(out, "%d %d\n255\n", v->width, v->height);
  fflush(out);
  for (int y = v->height - 1; y >= 0; --y) {
    fwrite(buffer + y * v->width, v->width, 1, out);
  }
  fflush(out);
}

void record_do(struct record *s, struct view *v) {
  if (s->screenshot || s->video) {
    if (! s->buffer) {
      s->bytes = v->width * v->height;
      s->buffer = (unsigned char *) malloc(s->bytes);
    }
    glReadPixels(0, 0, v->width, v->height, GL_RED, GL_UNSIGNED_BYTE, s->buffer);
    if (s->screenshot) {
      char filename[1024];
      snprintf(filename, 1000, "%04d.pgm", s->seqno++);
      FILE *out = fopen(s->filename ? s->filename : filename, "wb");
      if (! out) {
        fprintf(stderr, "\aERROR: screenshot failed\n");
      } else {
        record_write(out, s->buffer, v);
        fclose(out);
      }
      s->screenshot = false;
    }
    if (s->video) {
      record_write(stdout, s->buffer, v);
    }
  }
}
