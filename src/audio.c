#include "audio.h"

static inline float mtoi(float m, float sr) {
  return 440.0f * powf(2.0f, (m - 69.0f) / 12.0f) / sr;
}

static inline float mix(float x, float y, float f) {
  return x * f + (1 - f) * y;
}
static inline float wmix(float x, float y, float f) {
  if (y < x) { return mix(x, y + 1, f); }
  else       { return mix(x, y, f); }
}

int audio_process(jack_nframes_t nframes, void *arg) {
  struct audio *s = (struct audio *) arg;
  jack_default_audio_sample_t *out[4];
  out[0] = (jack_default_audio_sample_t *) jack_port_get_buffer(s->port[0], nframes);
  out[1] = (jack_default_audio_sample_t *) jack_port_get_buffer(s->port[1], nframes);
  out[2] = (jack_default_audio_sample_t *) jack_port_get_buffer(s->port[2], nframes);
  out[3] = (jack_default_audio_sample_t *) jack_port_get_buffer(s->port[3], nframes);
  float *phase = s->state, df = s->df, sr = 48000.0f, m[2], p[4], Q[2];
  int j = s->j, d = s->d, dd = 2 * (s->d + 1);
  m[0] = s->mouse[0];
  m[1] = s->mouse[1];
  p[0] = s->param[0];
  p[1] = s->param[1];
  p[2] = s->param[2];
  p[3] = s->param[3];
  for (jack_nframes_t i = 0; i < nframes; ++i) {
#define P(w,t) phase[(2 * (t) + w + dd)%dd]
    Q[0] = fmodf(P(0,j) + mtoi(p[0] + p[2] * (out[0][i] = cosf(6.283185307179586f * wmix(P(1,j-d), P(1,j-d+1), df))), sr), 1.0f);
    Q[1] = fmodf(P(1,j) + mtoi(p[1] + p[3] * (out[1][i] = cosf(6.283185307179586f * wmix(P(0,j-d), P(0,j-d+1), df))), sr), 1.0f);
#define NOISE (0.00001f * (rand() / (float)RAND_MAX - 0.5f))
    P(0,j+1) = Q[0] + NOISE;
    P(1,j+1) = Q[1] + NOISE;
#undef NOISE
#undef P
    out[2][i] = m[0];
    out[3][i] = m[1];
    j = (j + 1) % (d + 1);
  }
  s->j = j;
  return 0;
}

void audio_do(struct audio *s, glm::vec2 &m, glm::vec4 &p) {
  s->mouse[0] = m[0];
  s->mouse[1] = m[1];
  s->param[0] = p[0];
  s->param[1] = p[1];
  s->param[2] = p[2];
  s->param[3] = p[3];
}

void audio_begin(struct audio *s, int d, float df) {
  s->mouse[0] = 0;
  s->mouse[1] = 0;
  s->param[0] = 48;
  s->param[1] = 48;
  s->param[2] = 0;
  s->param[3] = 0;
  s->d = d;
  s->df = df;
  s->j = 0;
  s->state = (float *) calloc(2 * (d + 1), sizeof(float));
  if ((s->client = jack_client_open("lyapunov-fm", JackNoStartServer, 0))) {
    jack_set_process_callback(s->client, audio_process, s);
    s->port[0] = jack_port_register(s->client, "output_1", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    s->port[1] = jack_port_register(s->client, "output_2", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    s->port[2] = jack_port_register(s->client, "output_3", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    s->port[3] = jack_port_register(s->client, "output_4", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    if (jack_activate(s->client)) {
      fprintf (stderr, "cannot activate JACK client\n");
    } else {
      const char **ports;
      if ((ports = jack_get_ports(s->client, NULL, NULL, JackPortIsPhysical | JackPortIsInput))) {
        for (int i = 0; i < 2; ++ i) {
          if (! ports[i]) {
            break;
          }
          if (jack_connect(s->client, jack_port_name(s->port[i]), ports[i])) {
            fprintf(stderr, "cannot connect JACK output port\n");
          }
        }
        free(ports);
      }
    }
  }
}

void audio_end(struct audio *s) {
  if (s->client) {
    jack_client_close(s->client);
  }
}
