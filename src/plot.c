#include "plot_vert.glsl.c"
#include "plot_frag.glsl.c"

struct plot {
  GLuint program;
  GLint mvp;
  GLint g0;
  GLint q0;
  GLuint vao;
};

void plot_begin(struct plot *s, GLuint vbo, struct dvariable *d) {
  s->program = 0;
  s->mvp = -1;
  s->g0 = -1;
  s->q0 = -1;
  s->vao = 0;
  s->program = compile_program("plot", plot_vert, 0, plot_frag);
  s->mvp = glGetUniformLocation(s->program, "mvp");D;
  s->g0 = glGetAttribLocation(s->program, "g0");D;
  s->q0 = glGetAttribLocation(s->program, "q0");D;
  glGenVertexArrays(1, &s->vao);D;
  glBindVertexArray(s->vao);D;
  glBindBuffer(GL_ARRAY_BUFFER, vbo);D;
  glEnableVertexAttribArray(s->g0);D;
  glEnableVertexAttribArray(s->q0);D;
  glVertexAttribPointer(s->g0, 4, GL_FLOAT, GL_FALSE, d->stride, d->gptr);D;
  glVertexAttribPointer(s->q0, 2, GL_FLOAT, GL_FALSE, d->stride, d->qptr);D;
  glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  glBindVertexArray(0);D;
}

void plot_end(struct plot *s) {
  glDeleteProgram(s->program);D;
  s->program = 0;
  glDeleteVertexArrays(1, &s->vao);D;
  s->vao = 0;
}

void plot_start(struct plot *s, GLuint fbo, GLuint tex, struct view *v) {
  glUseProgram(s->program);D;
  float aspect = v->width * 1.0f / v->height;
  float tw = 2.0f * aspect / v->tilesy;
  float th = 2.0f          / v->tilesy;
  float tx = (v->tilex - v->tilesx / 2.0) * tw;
  float ty = (v->tiley - v->tilesy / 2.0) * th;
  glm::mat4 mvp = glm::ortho(tx, tx + tw, ty, ty + th);
  glUniformMatrix4fv(s->mvp, 1, GL_FALSE, &mvp[0][0]);D;
  glUseProgram(0);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);D;
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex, 0);D;
  GLenum buffers[1] = { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers(1, buffers);D;
}

void plot_finish(struct plot *s) {
  (void) s;
  glBindFramebuffer(GL_FRAMEBUFFER, 0);D;
}

void plot_do(struct plot *s, int count) {
  glUseProgram(s->program);D;
  glBindVertexArray(s->vao);D;
  glDrawArrays(GL_POINTS, 0, count);D;
  glBindVertexArray(0);D;
  glUseProgram(0);D;
}
