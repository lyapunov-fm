#ifndef BATCH_H
#define BATCH_H 1

struct batch {
  int version;
  char *line;
  size_t size;
  struct view v;
  int size_specified;
};

void batch_begin(struct batch *b);
void batch_do(struct batch *b, struct record *rec);

#endif
