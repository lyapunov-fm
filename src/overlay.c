#include <stdio.h>
#include <stdlib.h>

static inline int min(int x, int y) { return x < y ? x : y; }
static inline int max(int x, int y) { return x > y ? x : y; }

int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  // read input header
  int w, h;
  if (2 != scanf("P5\n#%*[^\n]\n%d %d 255\n", &w, &h)) { return 1; }
  // allocate buffers
  unsigned char *pgm = calloc(1, w * h);
  if (! pgm) { return 1; }
  unsigned char *ppm = calloc(1, w * h * 3);
  if (! ppm) { return 1; }
  // read input image
  if (1 != fread(pgm, w * h, 1, stdin)) { return 1; }
  // copy image
  for (int i = 0, j = 0; i < w * h; ++i) {
    unsigned char g = pgm[i];
    ppm[j++] = g; ppm[j++] = g; ppm[j++] = g;
  }
  for (int f = 0; f < w; ++f) {
    // draw crosshair
    int x, y;
    y = h / 2;
    for (x = max(0, f - 8); x <= min(f + 8, w - 1); ++x) {
      if (x == f) { continue; }
      int j = 3 * (w * y + x);
      ppm[j++] = 255; ppm[j++] = 0; ppm[j++] = 0;
    }
    x = f;
    for (y = max(0, h/2 - 8); y <= min(h/2 + 8, h - 1); ++y) {
      if (y == h/2) { continue; }
      int j = 3 * (w * y + x);
      ppm[j++] = 255; ppm[j++] = 0; ppm[j++] = 0;
    }
    // write image
    fprintf(stdout, "P6\n%d %d\n255\n", w, h);
    fwrite(ppm, w * h * 3, 1, stdout);
    fflush(stdout);
    // erase crosshair
    y = h/2;
    for (x = max(0, f - 8); x <= min(f + 8, w - 1); ++x) {
      if (x == f) { continue; }
      int i = w * y + x;
      int j = 3 * i;
      unsigned char g = pgm[i];
      ppm[j++] = g; ppm[j++] = g; ppm[j++] = g;
    }
    x = f;
    for (y = max(0, h/2 - 8); y <= min(h/2 + 8, h - 1); ++y) {
      if (y == h/2) { continue; }
      int i = w * y + x;
      int j = 3 * i;
      unsigned char g = pgm[i];
      ppm[j++] = g; ppm[j++] = g; ppm[j++] = g;
    }
  }
  return 0;
}
