void batch_begin(struct batch *b) {
  b->version = -1;
  b->size = 0;
  view_init(&b->v, 1024, 576, 120, 120, 0, 0, 72, 0);
  b->size_specified = 0;
}

void batch_done(struct render *r, void *v) {
  (void) r;
  struct batch *b = (struct batch *) v;
  S.record.screenshot = 1;
  record_do(&S.record, &b->v);
  batch_do(b, &S.record);
}

void batch_do(struct batch *b, struct record *rec) {
  int len;
#define L if (-1 == getline(&b->line, &b->size, stdin)) { exit(0); } len = strlen(b->line); if (b->line[len - 1] == '\n') { b->line[len - 1] = 0; len--; } fprintf(stderr, "%s\n", b->line);
  char *rest = 0;
#define S0(str) if (0 == strncmp(str, b->line, strlen(str))) { rest = b->line + strlen(str);
#define S(str) } else S0(str)
#define F(fld) sscanf(rest, " = %f", &b->v.fld);
  L
  S0("lyapunov-fm") if (0 == strcmp("/0", rest)) { b->version = 0; L } else { fprintf(stderr, "bad version: %s\n", b->line); exit(1); }
  } while (1) {
    S0("geometry") sscanf(rest, " = %dx%d", &b->v.width, &b->v.height); b->size_specified = 1;
    S("tiling")    sscanf(rest, " = %dx%d+%d+%d", &b->v.tilesx, &b->v.tilesy, &b->v.tilex, &b->v.tiley);
    S("delay")     F(delay)
    S("radius")    F(radius)
    S("note_x")    F(center[0])
    S("note_y")    F(center[1])
    S("index_x")   F(center[2])
    S("index_y")   F(center[3])
    S("matrix")    sscanf(rest, " = [ %f, %f, %f, %f ; %f, %f, %f, %f ]"
      , &b->v.matrix[0][0], &b->v.matrix[0][1], &b->v.matrix[0][2], &b->v.matrix[0][3]
      , &b->v.matrix[1][0], &b->v.matrix[1][1], &b->v.matrix[1][2], &b->v.matrix[1][3]);
    S("render")    if (rest[0] == '(' && rest[strlen(rest) - 1] == ')') { rest[strlen(rest) - 1] = 0; if (rec->filename) { free(rec->filename); } rec->filename = strdup(rest + 1); break; }
    } else { fprintf(stderr, "warning: ignoring: %s\n", b->line);
    }
    L
  }
#undef S0
#undef S
#undef F
#undef L
  render_ondone(&S.render, batch_done, b);
  render_do(&S.render, &b->v);
}

void batch_reshape(struct batch *b, int w, int h) {
  view_reshape(&b->v, w, h);
}
