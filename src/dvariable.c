void dvariable_begin(struct dvariable *s, int d) {
  s->d = d;
  s->packed = (d >> 1) + 1;
  s->nvaryings = s->packed + 2;
  s->attributes = (GLchar **) calloc(s->nvaryings, sizeof(GLchar *));
  s->varyings = (GLchar **) calloc(s->nvaryings, sizeof(GLchar *));
  for (int i = 0; i < s->packed; ++i) {
    s->attributes[i] = (GLchar *) calloc(1, 8);
    snprintf(s->attributes[i], 7, "p%d_i", i);
    s->varyings[i] = (GLchar *) calloc(1, 8);
    snprintf(s->varyings[i], 7, "p%d_o", i);
  }
  s->attributes[s->packed    ] = strdup("g_i");
  s->attributes[s->packed + 1] = strdup("q_i");
  s->varyings[s->packed    ] = strdup("g_o");
  s->varyings[s->packed + 1] = strdup("q_o");
  s->stride = (4 * (s->packed + 1) + 2) * sizeof(GLfloat);
  s->gptr = ((GLbyte *)0)+4*s->packed*sizeof(GLfloat);
  s->qptr = ((GLbyte *)0)+4*(s->packed+1)*sizeof(GLfloat);
}

void dvariable_setpointers(struct dvariable *s, GLuint program) {
  for (int i = 0; i < s->packed; ++i) {
    GLint p0 = glGetAttribLocation(program, s->attributes[i]);D;
    glEnableVertexAttribArray(p0);D;
    glVertexAttribPointer(p0, 4, GL_FLOAT, GL_FALSE, s->stride, ((GLbyte *)0)+ i*(4*sizeof(GLfloat)));D;
  }
  GLint g0 = glGetAttribLocation(program, "g_i");D;
  glEnableVertexAttribArray(g0);D;
  glVertexAttribPointer(g0, 4, GL_FLOAT, GL_FALSE, s->stride, s->gptr);D;
  GLint q0 = glGetAttribLocation(program, "q_i");D;
  glEnableVertexAttribArray(q0);D;
  glVertexAttribPointer(q0, 2, GL_FLOAT, GL_FALSE, s->stride, s->qptr);D;
}
