#include "preamble.glsl.c"

void debug_program(GLuint program, const char *name, const GLchar *vert, const GLchar *geom, const GLchar *frag) {
  if (program) {
    GLint linked = GL_FALSE;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);D;
    if (linked != GL_TRUE) {
      fprintf(stderr, "%s: link failed\n", name);
    }
    int length;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);D;
    char *buffer = (char *) malloc(length + 1);
    glGetProgramInfoLog(program, length, NULL, buffer);D;
    buffer[length] = 0;
    if (buffer[0]) {
      fprintf(stderr, "==== %s.log  ====\n%s\n", name, buffer);
      if (vert) { fprintf(stderr, "---- %s.vert ----\n%s\n", name, vert); }
      if (geom) { fprintf(stderr, "---- %s.geom ----\n%s\n", name, geom); }
      if (frag) { fprintf(stderr, "---- %s.frag ----\n%s\n", name, frag); }
    }
    free(buffer);
  } else {
    fprintf(stderr, "%s: program failed\n", name);
  }
}

void compile_shader(GLint program, GLenum type, const GLchar *source) {
  const GLchar *sources[2] = { preamble, source };
  GLuint shader = glCreateShader(type);D;
  glShaderSource(shader, 2, sources, 0);D;
  glCompileShader(shader);D;
  glAttachShader(program, shader);D;
  glDeleteShader(shader);D;
}

GLint compile_program(const char *name, const GLchar *vert, const GLchar *geom, const GLchar *frag, int nvaryings, GLchar **varyings) {
  GLint program = glCreateProgram();D;
  if (vert) { compile_shader(program, GL_VERTEX_SHADER  , vert); }
  if (geom) { compile_shader(program, GL_GEOMETRY_SHADER, geom); }
  if (frag) { compile_shader(program, GL_FRAGMENT_SHADER, frag); }
  if (nvaryings) {
    glTransformFeedbackVaryings(program, nvaryings, (const GLchar **) varyings, GL_INTERLEAVED_ATTRIBS);D;
  }
  glLinkProgram(program);D;
  debug_program(program, name, vert, geom, frag);
  return program;
}

GLint compile_program(const char *name, const GLchar *vert, const GLchar *geom, const GLchar *frag) {
  return compile_program(name, vert, geom, frag, 0, 0);
}
