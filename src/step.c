struct step {
  GLuint program;
  GLint sr;
  GLint iters;
  GLint perturb;
  GLint dither;
  GLint model;
  GLint center;
  GLint radius;
  GLint keep;
  GLuint vao;
  float dithersize;
  int d;
  GLfloat *noise;
};

void step_begin(struct step *s, GLuint vbo, struct dvariable *d, float df) {
  s->program = 0;
  s->sr = -1;
  s->iters = -1;
  s->perturb = -1;
  s->dither = -1;
  s->model = -1;
  s->center = -1;
  s->radius = -1;
  s->keep = -1;
  s->vao = 0;
  s->dithersize = 0;
  s->d = d->d;

  int len = 65536;
  char *src = (char *) calloc(1, len);
  int slen = 0;
#define L0(str) slen += snprintf(src + slen, len - slen - 1, str)
#define L1(fmt,arg) slen += snprintf(src + slen, len - slen - 1, fmt, arg)
#define L2(fmt,arg1,arg2) slen += snprintf(src + slen, len - slen - 1, fmt, arg1, arg2)

L1("const int D = %d;\n", d->d + 1);
L1("const float df = %f;\n", df);
L0("uniform float sr;\n");
L0("uniform int iters;\n");
L1("uniform vec2 perturb[%d];\n", d->d + 1);
L0("uniform vec2 dither;\n");
L0("uniform bool keep;\n");
L0("uniform mat4 model;\n");
L0("uniform vec4 center;\n");
L0("uniform float radius;\n");
for (int i = 0; i < d->packed; ++i) {
  L1("in vec4 p%d_i;\n", i);
}
L0("in vec4 g_i;\n");
L0("in vec2 q_i;\n");
for (int i = 0; i < d->packed; ++i) {
  L1("flat out vec4 p%d_o;\n", i);
}
L0("flat out vec4 g_o;\n");
L0("flat out vec2 q_o;\n");
L0("float delta(vec2 l, vec2 r) {\n");
L0("  vec2 d = l - r;\n");
L0("  d = abs(d);\n");
L0("  d = min(d, abs(d - vec2(1.0)));\n");
L0("  return dot(d, d);\n");
L0("}\n");
L0("vec2 mtoi(vec2 m) {\n");
L0("  return 440.0 * pow(vec2(2.0), (m - vec2(69.0)) / 12.0) / sr;\n");
L0("}\n");
L0("float wmix(float x, float y, float f) {\n");
L0("  if (y < x) { return mix(x, y + 1.0, f); }\n");
L0("  else       { return mix(x, y, f); }\n");
L0("}\n");
L0("vec2 wmix(vec2 x, vec2 y, float f) {\n");
L0("  return vec2(wmix(x.x, y.x, f), wmix(x.y, y.y, f));\n");
L0("}\n");
L0("\n");
L0("vec2 step(vec2 z, vec2 zz, vec4 a1) {\n");
L0("  return fract(z.xy + mtoi(a1.xy + a1.zw * cos(6.283185307179586 * zz.yx)));\n");
L0("}\n");
L0("void main() {\n");
L0("// state vector\n");
L0("  vec2 l[D];\n");
L0("  vec2 r[D];\n");
L0("\n");
L0("  // unpack state vector\n");
for (int i = 0; i < d->packed; ++i) {
  L2("  r[%d] = p%d_i.xy;\n", 2 * i, i);
  if (2 * i < d->d) {
    L2("  r[%d] = p%d_i.zw;\n", 2 * i + 1, i);
  }
}
L0("\n");
L0("  // perturb\n");
L0("  for (int i = 0; i < D; ++i) {\n");
L0("    l[i] = r[i] + perturb[i];\n");
L0("  }\n");
L0("\n");
L0("  // maximum separation\n");
L0("  float sM = float(D) * 0.25;\n");
L0("  // initial separation\n");
L0("  float s0 = 0.0;\n");
L0("  for (int i = 0; i < D; ++i) {\n");
L0("    s0 += delta(l[i], r[i]);\n");
L0("  }\n");
L0("\n");
L0("  // parameter\n");
L0("  vec4 a1 = model * (vec4(radius * (q_i + dither), 0.0, 0.0)) + center;\n");
L0("\n");
L0("  // step\n");
L0("  vec3 f = vec3(g_i.xyz);\n");
L0("  int j = 0;\n");
L0("  for (int h = 0; h < iters/D + 1; ++h) {\n");
L0("    if (s0 > sM * 0.25) { break; }\n");
L0("    for (int i = 0; i < D; ++i) {\n");
L0("      int jj = (j + 1) %% D;\n");
L0("      int jjj = (j + 2) %% D;\n");
L0("      l[jj] = step(l[j], wmix(l[jj], l[jjj], df), a1);\n");
L0("      r[jj] = step(r[j], wmix(r[jj], r[jjj], df), a1);\n");
L0("      j = jj;\n");
L0("    }\n");
L0("\n");
L0("    // final separation\n");
L0("    float s1 = 0.0;\n");
L0("    for (int i = 0; i < D; ++i) {\n");
L0("      s1 += delta(l[i], r[i]);\n");
L0("    }\n");
L0("    // accumulate lyapunov exponent\n");
L0("    if (keep && s0 > 0.0 && s1 > 0.0) {\n");
L0("      float k = 0.5 * log(s1 / s0) / float(D);\n");
L0("      if (! ( 10.0 >= k)) { k =  10.0; }\n");
L0("      if (! (-10.0 <= k)) { k = -10.0; }\n");
L0("      f += vec3(1.0, k, k * k);\n");
L0("    }\n");
L0("    s0 = s1;\n");
L0("  }\n");
L0("  g_o = vec4(f, g_i.w);\n");
L0("\n");
L0("  // pass through pixel coords\n");
L0("  q_o = q_i;\n");
L0("\n");
L0("  // pack state vector\n");
L0("  // explore more orbits by picking the perturbed version\n");
for (int i = 0; i < d->packed; ++i) {
  L2("  p%d_o.xy = l[(j + %d) %% D];\n", i, 2 * i);
  if (2 * i < d->d) {
    L2("  p%d_o.zw = l[(j + %d) %% D];\n", i, 2 * i + 1);
  }
}
L0("\n");
L0("}\n");

#undef L0
#undef L1
#undef L2

  GLchar *step_vert = src;

  s->program = compile_program("step", step_vert, 0, 0, d->nvaryings, d->varyings);
  s->sr = glGetUniformLocation(s->program, "sr");D;
  s->iters = glGetUniformLocation(s->program, "iters");D;
  s->perturb = glGetUniformLocation(s->program, "perturb");D;
  s->noise = (GLfloat *) calloc(1, 2 * (s->d + 1) * sizeof(GLfloat));
  s->dither = glGetUniformLocation(s->program, "dither");D;
  s->model = glGetUniformLocation(s->program, "model");D;
  s->center = glGetUniformLocation(s->program, "center");D;
  s->radius = glGetUniformLocation(s->program, "radius");D;
  s->keep = glGetUniformLocation(s->program, "keep");D;
  glGenVertexArrays(1, &s->vao);D;
  glBindVertexArray(s->vao);D;
  glBindBuffer(GL_ARRAY_BUFFER, vbo);D;
  dvariable_setpointers(d, s->program);D;
  glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  glBindVertexArray(0);D;
}

void step_end(struct step *s) {
  glDeleteProgram(s->program);D;
  s->program = 0;
  glDeleteVertexArrays(1, &s->vao);D;
  s->vao = 0;
}

void step_start(struct step *s, double samplerate, struct view *v) {
  glUseProgram(s->program);D;
  glUniform1f(s->sr, samplerate);D;
  glUniform1i(s->iters, 360);D;
  glUniformMatrix4fv(s->model, 1, GL_FALSE, &v->matrix[0][0]);D;
  glUniform4fv(s->center, 1, &v->center[0]);D;
  glUniform1f(s->radius, v->radius);D;
  s->dithersize = 2.0f / (v->height * v->tilesy);
  glUseProgram(0);D;
}

void step_do(struct step *s, GLuint vbo, GLuint count, int keep) {
  glEnable(GL_RASTERIZER_DISCARD);
  glBindVertexArray(s->vao);D;
  glUseProgram(s->program);
  glUniform1i(s->keep, keep);D;
#define NOISE (0.00001 * (rand() / (double) RAND_MAX - 0.5))
  for (int i = 0; i < 2 * (s->d + 1); ++i) {
    s->noise[i] = NOISE;
  }
  glUniform2fv(s->perturb, s->d + 1, s->noise);D;
#undef NOISE
#define NOISE ((rand() / (double) RAND_MAX - 0.5) * s->dithersize)
  glUniform2f(s->dither, NOISE, NOISE);D;
#undef NOISE
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo);D;
  glBeginTransformFeedback(GL_POINTS);D;
  glDrawArrays(GL_POINTS, 0, count);D;
  glEndTransformFeedback();D;
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);D;
  glUseProgram(0);D;
  glBindVertexArray(0);D;
  glDisable(GL_RASTERIZER_DISCARD);
}
