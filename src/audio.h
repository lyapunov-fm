#ifndef AUDIO_H
#define AUDIO_H 1

#include <jack/jack.h>

struct audio {
  jack_client_t *client;
  jack_port_t *port[4];
  float mouse[2];
  float param[4];
  int d;
  float df;
  int j;
  float *state;
};

void audio_begin(struct audio *s, int d, float df);
void audio_end(struct audio *s);
void audio_do(struct audio *s, glm::vec2 &m, glm::vec4 &p);

#endif
