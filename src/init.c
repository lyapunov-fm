struct init {
  GLuint program;
  GLint c;
  GLuint vao;
};

void init_begin(struct init *s, struct dvariable *d) {
  s->program = 0;
  s->c = -1;
  s->vao = 0;
  int len = 65536;
  char *src = (char *) calloc(1, len);
  int slen = 0;
#define L0(str) slen += snprintf(src + slen, len - slen - 1, str)
#define L1(fmt,arg) slen += snprintf(src + slen, len - slen - 1, fmt, arg)
  L0("in  vec2 c;\n");
  for (int i = 0; i < d->packed; ++i) {
    L1("flat out vec4 %s;\n", d->varyings[i]);
  }
  L0("flat out vec4 g_o;\n");
  L0("flat out vec2 q_o;\n");
  L0("void main() {\n");
  for (int i = 0; i < d->packed; ++i) {
    L1("  %s = vec4(0.0);\n", d->varyings[i]);
  }
  L0("  g_o = vec4(0.0);\n");
  L0("  q_o = c;\n");
  L0("}\n");
#undef L0
#undef L1
  s->program = compile_program("init", src, 0, 0, d->nvaryings, d->varyings);
  s->c = glGetAttribLocation(s->program, "c");D;
  glGenVertexArrays(1, &s->vao);D;
}

void init_end(struct init *s) {
  glDeleteProgram(s->program);D;
  s->program = 0;
  glDeleteVertexArrays(1, &s->vao);D;
  s->vao = 0;
}

void init_do(struct init *s, GLuint vbo0, GLuint vbo1, struct view *v) {
  glEnable(GL_RASTERIZER_DISCARD);D;
  glBindVertexArray(s->vao);D;
  glBindBuffer(GL_ARRAY_BUFFER, vbo0);D;
  glUseProgram(s->program);D;
  glVertexAttribPointer(s->c, 2, GL_FLOAT, GL_FALSE, 0, 0);D;
  glEnableVertexAttribArray(s->c);D;
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo1);D;
  glBeginTransformFeedback(GL_POINTS);D;
  glDrawArrays(GL_POINTS, 0, v->width * v->height);D;
  glEndTransformFeedback();D;
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);D;
  glUseProgram(0);D;
  glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  glBindVertexArray(0);D;
  glDisable(GL_RASTERIZER_DISCARD);D;
}
