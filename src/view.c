void view_init(struct view *v, int width, int height, double centerx, double centery, double centerz, double centerw, double radius, double delay) {
  v->width = width;
  v->height = height;
  v->matrix = glm::mat4(0.0f);
  v->matrix[0][0] = 1;
  v->matrix[0][1] = 1;
  v->matrix[1][2] = 1;
  v->matrix[1][3] = 1;
  v->center[0] = centerx;
  v->center[1] = centery;
  v->center[2] = centerz;
  v->center[3] = centerw;
  v->radius = radius;
  v->tilex = 0;
  v->tiley = 0;
  v->tilesx = 1;
  v->tilesy = 1;
  v->delay = delay;
}

void view_copy(struct view *dst, struct view *src) {
  dst->width = src->width;
  dst->height = src->height;
  dst->matrix = src->matrix;
  dst->center = src->center;
  dst->radius = src->radius;
  dst->tilex = src->tilex;
  dst->tiley = src->tiley;
  dst->tilesx = src->tilesx;
  dst->tilesy = src->tilesy;
  dst->delay = src->delay;
}

void view_reshape(struct view *v, int width, int height) {
  v->width = width;
  v->height = height;
}

void view_reset(struct view *v, double centerx, double centery, double centerz, double centerw, double radius) {
  v->center[0] = centerx;
  v->center[1] = centery;
  v->center[2] = centerz;
  v->center[3] = centerw;
  v->radius = radius;
}

void view_plane(struct view *v, int which) {
  v->matrix = glm::mat4(0.0f);
  v->matrix[0][0] = 1;
  v->matrix[0][1] = which;
  v->matrix[1][2] = 1;
  v->matrix[1][3] = which;
}  

void view_coord(glm::vec4 *c, struct view *v, int px, int py) {
  glm::vec4 p = glm::vec4(float((px -  v->width / 2.0) * 2.0 / v->height), float((v->height / 2.0 - py) * 2.0 / v->height), 0.0f, 0.0f);
  *c = v->matrix * (v->radius * p) + v->center;
}

void view_center(struct view *v, glm::vec4 &c) {
  v->center = c;
}

void view_zoom(struct view *v, glm::vec4 &c, float z) {
  float g = pow(0.5, z);
  v->center = g * v->center + (1 - g) * c;
  v->radius *= g;
}

void view_zoom(struct view *v, float z) {
  view_zoom(v, v->center, z);
}
