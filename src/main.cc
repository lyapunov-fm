#define _POSIX_C_SOURCE 199309L

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <string.h>
#include <stdio.h>
#include <time.h>

#define D do{ int e = glGetError(); if (e != 0) { fprintf(stderr, "OpenGL error %d in %s() (line %d)\n", e, __FUNCTION__, __LINE__); } }while(0)

#include "utility.c"
#include "shader.c"
#include "record.c"

#include "dvariable.h"
#include "dvariable.c"
#include "fillc.c"
#include "init.c"
#include "step.c"
#include "prune.c"
#include "plot.c"
#include "colour.c"

#include "state.c"
void idlecb();
#include "interact.c"
#include "render.c"
#include "view.c"
#include "audio.c"
#include "batch.c"

void reshapecb(int new_width, int new_height) {
  glViewport(0, 0, new_width, new_height);

  if (S.record.buffer) {
    free(S.record.buffer);
    S.record.buffer = 0;
  }

  render_reshape(&S.render, new_width, new_height);
  if (S.mode == 0) {
    interact_reshape(new_width, new_height);
    render_do(&S.render, &I.view);
  } else {
    batch_reshape(&S.batch, new_width, new_height);
    render_do(&S.render, &S.batch.v);
  }

  glutIdleFunc(idlecb);
}

void begincb(int d, float df) {
  render_begin(&S.render, d, df);
  if (S.mode == 0) {
    audio_begin(&S.audio, d, df);
    interact_begin();
  } else {
    batch_begin(&S.batch);
  }
}

void endcb() {
  render_end(&S.render);
  if (S.mode == 0) {
    audio_end(&S.audio);
  }
}

void displaycb() {
  render_display(&S.render);
}

void idlecb() {
  render_idle(&S.render);
}

int main(int argc, char **argv) {
  srand(time(NULL));
  float delay = 1;
  if (argc > 1) {
    delay = atof(argv[1]);
  }
  int d = ceilf(delay);
  float df = d - delay;
  int mode = 0;
  if (argc > 2) {
    mode = 1;
  }
  state_init(mode, delay);
  glutInitWindowSize(512, 288);
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutCreateWindow(argv[0]);
  glewExperimental = GL_TRUE;
  glewInit();

  GLint n;
  glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &n);
  fprintf(stderr, "GL_MAX_VERTEX_ATTRIBS     = %d\n", n);
  glGetIntegerv(GL_MAX_VARYING_COMPONENTS, &n);
  fprintf(stderr, "GL_MAX_VARYING_COMPONENTS = %d\n", n);
  glGetIntegerv(GL_MAX_VARYING_VECTORS, &n);
  fprintf(stderr, "GL_MAX_VARYING_VECTORS    = %d\n", n);
  glGetIntegerv(GL_MAX_VARYING_FLOATS, &n);
  fprintf(stderr, "GL_MAX_VARYING_FLOATS     = %d\n", n);

  begincb(d, df);
  atexit(endcb);
  glutDisplayFunc(displaycb);
  glutReshapeFunc(reshapecb);

  if (mode != 0) {
    batch_do(&S.batch, &S.record);
  }
  glutMainLoop();
  return 0;
}
