uniform mat4 mvp;
in vec4 g0;
in vec2 q0;
flat out vec3 g;
void main() {
  g = g0.xyz;
  gl_Position = mvp * vec4(q0, 0.0, 1.0);
}
