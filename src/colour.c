#include "colour_vert.glsl.c"
#include "colour_frag.glsl.c"

struct colour {
  GLuint program;
  GLint mvp;
  GLint tex;
  GLint q0;
  GLuint vao;
  GLuint vbo;
};

void colour_begin(struct colour *s) {
  s->program = 0;
  s->mvp = -1;
  s->tex = -1;
  s->q0 = -1;
  s->vao = 0;
  s->vbo = 0;
  s->program = compile_program("colour", colour_vert, 0, colour_frag);
  s->mvp = glGetUniformLocation(s->program, "mvp");D;
  s->tex = glGetUniformLocation(s->program, "tex");D;
  s->q0 = glGetAttribLocation(s->program, "q0");D;
  glGenVertexArrays(1, &s->vao);D;
  glGenBuffers(1, &s->vbo);D;
}

void colour_end(struct colour *s) {
  glDeleteProgram(s->program);D;
  s->program = 0;
  glDeleteVertexArrays(1, &s->vao);D;
  s->vao = 0;
  glDeleteBuffers(1, &s->vbo);D;
  s->vbo = 0;
}

int cmp_float(const void *x, const void *y) {
  const float *a = (const float *) x;
  const float *b = (const float *) y;
  if (*a < *b) return -1;
  if (*a > *b) return  1;
  return 0;
}

void colour_do(struct colour *s, GLuint tex, int tex_size, struct view *v) {
  glBindTexture(GL_TEXTURE_2D, tex);D;
  glUseProgram(s->program);D;
  glm::mat4 mvp = glm::ortho(0.0f, 1.0f, 0.0f, 1.0f);
  glUniformMatrix4fv(s->mvp, 1, GL_FALSE, &mvp[0][0]);D;
  glUniform1i(s->tex, 0);D;
  GLfloat x = v->width  * 1.0 / tex_size;
  GLfloat y = v->height * 1.0 / tex_size;
  GLfloat quad[] = {
    0, 0,  0, 0,
    0, 1,  0, y,
    1, 0,  x, 0,
    1, 1,  x, y
  };
  glBindVertexArray(s->vao);D;
  glBindBuffer(GL_ARRAY_BUFFER, s->vbo);D;
  glBufferData(GL_ARRAY_BUFFER, 16 * sizeof(GLfloat), &quad, GL_STATIC_DRAW);D;
  glEnableVertexAttribArray(s->q0);D;
  glVertexAttribPointer(s->q0, 4, GL_FLOAT, GL_FALSE, 0, 0);D;
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);D;
  glDisableVertexAttribArray(s->q0);D;
  glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  glBindVertexArray(0);D;
  glUseProgram(0);D;
  glBindTexture(GL_TEXTURE_2D, 0);D;
}
