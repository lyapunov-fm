flat in vec3 g;
out layout(location = 0, index = 0) vec4 c;
void main() {
  c = vec4(g, 1.0);
}
