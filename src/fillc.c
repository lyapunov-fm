#include "fillc_vert.glsl.c"
#include "fillc_frag.glsl.c"

struct fillc {
  GLuint program;
  GLint  mvp;
  GLint  p;
  GLuint vao;
  GLuint vbo;
};

void fillc_begin(struct fillc *s) {
  s->program = 0;
  s->mvp = -1;
  s->p = -1;
  s->vbo = 0;
  s->vao = 0;
  s->program = compile_program("fillc", fillc_vert, 0, fillc_frag);
  s->mvp = glGetUniformLocation(s->program, "mvp");D;
  s->p = glGetAttribLocation(s->program, "p");D;
  glGenVertexArrays(1, &s->vao);D;
  glGenBuffers(1, &s->vbo);D;
}

void fillc_end(struct fillc *s) {
  glDeleteProgram(s->program);D;
  s->program = 0;
  glDeleteVertexArrays(1, &s->vao);D;
  s->vao = 0;
  glDeleteBuffers(1, &s->vbo);D;
  s->vbo = 0;
}

void fillc_do(struct fillc *s, GLuint vbo1, GLuint fbo, GLuint tex, struct view *v) {
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);D;
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex, 0);D;
  GLenum buffers[1] = { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers(1, buffers);D;
  glViewport(0, 0, v->width, v->height);D;
  glUseProgram(s->program);D;
  float aspect = v->width * 1.0f / v->height;
  float tw = 2.0f * aspect / v->tilesy;
  float th = 2.0f          / v->tilesy;
  float tx = (v->tilex - v->tilesx / 2.0) * tw;
  float ty = (v->tiley - v->tilesy / 2.0) * th;
  glm::mat4 mvp = glm::ortho(tx, tx + tw, ty, ty + th);
  glUniformMatrix4fv(s->mvp, 1, GL_FALSE, &mvp[0][0]);D;
  GLfloat quad[] = {
    tx,      ty,
    tx,      ty + th,
    tx + tw, ty,
    tx + tw, ty + th
  };
  glBindVertexArray(s->vao);D;
  glBindBuffer(GL_ARRAY_BUFFER, s->vbo);D;
  glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(GLfloat), &quad, GL_STATIC_DRAW);D;
  glVertexAttribPointer(s->p, 2, GL_FLOAT, GL_FALSE, 0, 0);D;
  glEnableVertexAttribArray(s->p);D;
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);D;
  glDisableVertexAttribArray(s->p);D;
  glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  glBindVertexArray(0);D;
  glUseProgram(0);D;
  // Copy texture to vertex buffer.
  glBindBuffer(GL_PIXEL_PACK_BUFFER, vbo1);D;
  glReadPixels(0, 0, v->width, v->height, GL_RG, GL_FLOAT, 0);D;
  glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);D;
  glClearColor(0,0,0,0);D;
  glClear(GL_COLOR_BUFFER_BIT);D;
  glBindFramebuffer(GL_FRAMEBUFFER, 0);D;
}
