struct prune {
  GLuint program;
  GLuint vao;
  GLuint query;
};

void prune_begin(struct prune *s, GLuint vbo, struct dvariable *d) {
  s->program = 0;
  s->vao = 0;
  s->query = 0;

  int len = 65536;
  char *src = (char *) calloc(1, len);
  int slen = 0;
#define L0(str) slen += snprintf(src + slen, len - slen - 1, str)
#define L1(fmt,arg) slen += snprintf(src + slen, len - slen - 1, fmt, arg)
#define L2(fmt,arg1,arg2) slen += snprintf(src + slen, len - slen - 1, fmt, arg1, arg2)

  for(int i = 0; i < d->packed; ++i) {
    L1("in vec4 p%d_i;\n", i);
  }
  L0("in vec4 g_i;\n");
  L0("in vec2 q_i;\n");
  for(int i = 0; i < d->packed; ++i) {
    L1("flat out vec4 p%d_v;\n", i);
  }
  L0("flat out vec4 g_v;\n");
  L0("flat out vec2 q_v;\n");
  L0("void main() {\n");
  for(int i = 0; i < d->packed; ++i) {
    L2("  p%d_v = p%d_i;\n", i, i);
  }
  L0("  g_v = g_i;\n");
  L0("  q_v = q_i;\n");
  L0("}\n");
  char *prune_vert = src;

  src = (char *) calloc(1, len);
  slen = 0;
  L0("layout(points) in;\n");
  L0("layout(points, max_vertices = 1) out;\n");
  for(int i = 0; i < d->packed; ++i) {
    L1("flat in vec4 p%d_v[1];\n", i);
  }
  L0("flat in vec4 g_v[1];\n");
  L0("flat in vec2 q_v[1];\n");
  for (int i = 0; i < d->packed; ++i) {
    L1("flat out vec4 p%d_o;\n", i);
  }
  L0("flat out vec4 g_o;\n");
  L0("flat out vec2 q_o;\n");
  L0("void main() {\n");
  L0("  float umean = (g_v[0].y + 10.0) / (g_v[0].x + 1.0);\n");
  L0("  float lmean = (g_v[0].y - 10.0) / (g_v[0].x + 1.0);\n");
  L0("  float delta = abs(tanh(8.0 * umean) - tanh(8.0 * lmean));\n");
  L0("  if (! (g_v[0].x >= 64.0 && delta < 0.5 / 256.0)) {\n");
  for (int i = 0; i < d->packed; ++i) {
    L2("  p%d_o = p%d_v[0];\n", i, i);
  }
  L0("    g_o = g_v[0];\n");
  L0("    q_o = q_v[0];\n");
  L0("    EmitVertex();\n");
  L0("    EndPrimitive();\n");
  L0("  }\n");
  L0("}\n");
  char *prune_geom = src;

#undef L0
#undef L1
#undef L2

  s->program = compile_program("prune", prune_vert, prune_geom, 0, d->nvaryings, d->varyings);
  glGenVertexArrays(1, &s->vao);D;
  glGenQueries(1, &s->query);D;
  glBindVertexArray(s->vao);D;
  glBindBuffer(GL_ARRAY_BUFFER, vbo);D;
  dvariable_setpointers(d, s->program);
  glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  glBindVertexArray(0);D;
}

void prune_end(struct prune *s) {
  glDeleteProgram(s->program);D;
  s->program = 0;
  glDeleteVertexArrays(1, &s->vao);D;
  s->vao = 0;
  glDeleteQueries(1, &s->query);D;
  s->query = 0;
}

void prune_do(struct prune *s, GLuint vbo, GLuint *count) {
  glEnable(GL_RASTERIZER_DISCARD);
  glBindVertexArray(s->vao);D;
  glUseProgram(s->program);D;
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo);D;
  glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, s->query);D;
  glBeginTransformFeedback(GL_POINTS);D;
  glDrawArrays(GL_POINTS, 0, *count);D;
  glEndTransformFeedback();D;
  glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);D;
  glGetQueryObjectuiv(s->query, GL_QUERY_RESULT, count);D;
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);D;
  glUseProgram(0);D;
  glBindVertexArray(0);D;
  glDisable(GL_RASTERIZER_DISCARD);
}
