uniform mat4 mvp;
in  vec2 p;
smooth out vec2 t;
void main() {
  gl_Position = mvp * vec4(p, 0.0, 1.0);
  t = p;
}
