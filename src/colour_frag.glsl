uniform sampler2D tex;
smooth in vec2 t;
out layout(location = 0, index = 0) vec4 c;
void main() {
  vec3 g = texture(tex, t).xyz;
  float mean = 0.0;
  if (g.x > 0.0) { mean = g.y / g.x; }
  float umean = (g.y + 10.0) / (g.x + 1.0);
  float lmean = (g.y - 10.0) / (g.x + 1.0);
  float delta = abs(tanh(8.0 * umean) - tanh(8.0 * lmean));
  if (delta < 0.5 / 256.0) {
    c = vec4(vec3(0.5 + 0.5 * tanh(8.0 * mean)), 1.0);
  } else {
    c = vec4(vec3(0.5 + 0.5 * tanh(8.0 * mean)), 1.0) * vec4(1.0, 0.7, 0.7, 1.0);
  }
/*
  if (l <= -10.0) {
    c = vec4(0.0, 0.0, 0.0, 1.0);
  } else {
    c = 0.5 * mix(vec4(1.0 - tanh(l * 4.0), 1.0 - tanh(l * 3.0), 0.0, 2.0),
            vec4(0.0, 0.0, tanh(l * 3.0) + 1.0, 2.0), 0.5 + 0.5 * tanh(l));
  }
*/
}
