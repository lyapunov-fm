template <typename T> T& max(T& a, T& b) {
  return a > b ? a : b;
}

int ceil2n(int z) {
  int n = 1;
  while (0 < n && n < z) {
    n <<= 1;
  }
  return n;
}

double time_difference(const struct timespec *t1, const struct timespec *t0) {
  return (t1->tv_sec - t0->tv_sec) + (t1->tv_nsec - t0->tv_nsec) / 1000000000.0;
}
