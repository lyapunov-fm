#ifndef DVARIABLE_H
#define DVARIABLE_H 1

struct dvariable {
  int d;
  int packed;
  int nvaryings;
  GLchar **attributes; // p%d_i, g_i, q_i
  GLchar **varyings;   // p%d_o, g_o, q_o
  int stride;
  void *gptr;
  void *qptr;
};

void dvariable_begin(struct dvariable *s, int d);
void dvariable_setpointers(struct dvariable *s, GLuint program);

#endif
