void render_begin(struct render *s, int d, float df) {
  s->reshape_requested = 0;
  s->win_width = -1;
  s->win_height = -1;
  s->tex_size = -1;
  s->vbo_size = -1;
  s->tex = 0;
  s->fbo = 0;
  s->vbo[0] = 0;
  s->vbo[1] = 0;
  s->count = 0;
  s->steps = 0;
  s->samplerate = 48000.0;

  glClampColor(GL_CLAMP_VERTEX_COLOR, GL_FALSE);D;
  glClampColor(GL_CLAMP_READ_COLOR, GL_FALSE);D;
  glClampColor(GL_CLAMP_FRAGMENT_COLOR, GL_FALSE);D;
  glGenTextures(1, &s->tex);D;
  glBindTexture(GL_TEXTURE_2D, s->tex);D;
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);D;
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);D;
  glBindTexture(GL_TEXTURE_2D, 0);D;
  glGenFramebuffers(1, &s->fbo);D;
  glGenBuffers(2, s->vbo);D;

  dvariable_begin(&s->dvariable, d);
  fillc_begin(&s->fillc);
  init_begin(&s->init, &s->dvariable);
  step_begin(&s->step, s->vbo[0], &s->dvariable, df);
  prune_begin(&s->prune, s->vbo[1], &s->dvariable);
  plot_begin(&s->plot, s->vbo[1], &s->dvariable);
  colour_begin(&s->colour);

  s->idle.f = 0;
  s->done.f = 0;
  s->done.u = 0;
}

void render_end(struct render *s) {
  glDeleteTextures(1, &s->tex);D;
  glDeleteFramebuffers(1, &s->fbo);D;
  glDeleteBuffers(2, s->vbo);D;
  fillc_end(&s->fillc);
  init_end(&s->init);
  step_end(&s->step);
  prune_end(&s->prune);
  plot_end(&s->plot);
  colour_end(&s->colour);
}

void render_reshape(struct render *s, int w, int h) {
  s->win_width  = w;
  s->win_height = h;
  if (s->reshape_requested) {
    s->reshape_requested = 0;
    s->view.width  = w;
    s->view.height = h;
  }
}

void render_go(struct render *s);
void render_do(struct render *s, struct view *v) {
  if (v->width != s->win_width || v->height != s->win_height) {
    if (! s->reshape_requested)  {
      s->reshape_requested = 1;
      glutReshapeWindow(v->width, v->height);
    }
    return;
  }
  int new_count = v->width * v->height;
  int new_size = ceil2n(max(v->width, v->height));
  bool reallocate = new_size != s->tex_size;
  if (reallocate) {
    s->tex_size = new_size;
    glBindTexture(GL_TEXTURE_2D, s->tex);D;
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, s->tex_size, s->tex_size, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);D;
    glBindTexture(GL_TEXTURE_2D, 0);D;
  }
  if (s->vbo_size != new_count) {
    s->vbo_size = new_count;
    for (int i = 0; i < 2; ++i) {
      glBindBuffer(GL_ARRAY_BUFFER, s->vbo[i]);D;
      glBufferData(GL_ARRAY_BUFFER, s->vbo_size * s->dvariable.stride, 0, GL_DYNAMIC_COPY);D;
      glBindBuffer(GL_ARRAY_BUFFER, 0);D;
    }
  }
  view_copy(&s->view, v);
  s->count = s->view.width * s->view.height;
  s->steps = 0;
  s->total = 0;
  clock_gettime(CLOCK_MONOTONIC, &s->t_start);
  clock_gettime(CLOCK_MONOTONIC, &s->t_update);
  fillc_do(&s->fillc, s->vbo[1], s->fbo, s->tex, &s->view);
  init_do(&s->init, s->vbo[1], s->vbo[0], &s->view);
  step_start(&s->step, s->samplerate, &s->view);
  s->idle.f = render_go;
  glutIdleFunc(idlecb);
}

void render_go(struct render *s) {
  plot_start(&s->plot, s->fbo, s->tex, &s->view);

#define GO \
  s->total += s->count; \
  step_do(&s->step, s->vbo[1], s->count, s->steps++ > 128); \
  plot_do(&s->plot, s->count); \
  prune_do(&s->prune, s->vbo[0], &s->count);

  glFinish();
  struct timespec t0;
  clock_gettime(CLOCK_MONOTONIC, &t0);

  GO

  glFinish();
  struct timespec t1;
  clock_gettime(CLOCK_MONOTONIC, &t1);

  double dt = time_difference(&t1, &t0);

  for (double t = dt; t < 0.04 && s->count /* && s->total < 8192 * s->view.width * s->view.height */ ; t += dt) {
    GO
  }

#undef GO

  plot_finish(&s->plot);
  if (! (s->count /* && s->total < 8192 * s->view.width * s->view.height */)) {
    s->idle.f = 0;
  }
}

void render_idle(struct render *s) {
  if (s->idle.f) {
    s->idle.f(s);
  } else {
    glutIdleFunc(0);
  }
  glutPostRedisplay();
  glutReportErrors();
}

void render_display(struct render *s) {
  struct timespec t;
  clock_gettime(CLOCK_MONOTONIC, &t);
  if (time_difference(&t, &s->t_update) > 1 || ! s->count) {
    fprintf(stderr, "\r%8d%12d    %4.3f", s->count, s->total, time_difference(&t, &s->t_start)); fflush(stderr);
    clock_gettime(CLOCK_MONOTONIC, &s->t_update);
    if (! s->count) {
      fprintf(stderr, "\n");
    }
  }
  colour_do(&s->colour, s->tex, s->tex_size, &s->view);
  glutSwapBuffers();
  if (! s->idle.f) {
    if (s->done.f) {
      s->done.f(s, s->done.u);
    }
  }
}

void render_ondone(struct render *s, void (*f)(struct render *, void *), void *u) {
  s->done.f = f;
  s->done.u = u;
}
