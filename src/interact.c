static struct {
  int which;
  struct view view;
} I;

void interact_mouse(int button, int state, int x, int y) {
  if (state == GLUT_DOWN) {
    glm::vec4 v;
    view_coord(&v, &I.view, x, y);
    if (button == GLUT_MIDDLE_BUTTON) { view_center(&I.view, v);
    } else if (button == GLUT_LEFT_BUTTON  || button == 3) { view_zoom(&I.view, v,  1);
    } else if (button == GLUT_RIGHT_BUTTON || button == 4) { view_zoom(&I.view, v, -1);
    }
    render_do(&S.render, &I.view);
  }
}

void interact_motion(int x, int y) {
  glm::vec4 v;
  view_coord(&v, &I.view, x, y);
  glm::vec2 m;
  m[0] = x / (float) I.view.width;
  m[1] = y / (float) I.view.height;
  audio_do(&S.audio, m, v);
}

void interact_automate(int x) {
  if (x < I.view.width) {
    interact_motion(x, I.view.height / 2);
    glutTimerFunc(40, interact_automate, x + 1);
  } else {
    interact_motion(0, I.view.height / 2);
    jack_transport_stop(S.audio.client);
  }
}

void interact_keyboard(unsigned char key, int x, int y) {
  (void) x;
  (void) y;
  if (key == 27 || key == 'q') {
    exit(0);
  }
  if (S.mode != 0) return;
  if (key == '\t') {
    I.which = -I.which;
    view_plane(&I.view, I.which);
    render_do(&S.render, &I.view);
/*
  } else if (key == 'a') {
    S.record.screenshot = true;
    glutPostRedisplay();
    jack_transport_start(S.audio.client);
    glutTimerFunc(40, interact_automate, 0);
  } else if (key == 'l') {
    S.animate.active = 1;
    S.render.view.which = 2;
    glutPostRedisplay();
*/
  } else if (key == 's') {
    S.record.screenshot = true;
    glutPostRedisplay();
/*
  } else if (key == 'r') {
    S.record.video = !S.record.video;
    glutPostRedisplay();
  } else if (key == '@') {
    S.render.idle.f = 0;
    S.rendering = true;
*/
  }
}

void interact_special(int key, int x, int y) {
  (void) x;
  (void) y;
  if (key == GLUT_KEY_F11)               { glutFullScreenToggle(); return;
  } else if (key == GLUT_KEY_HOME)      { view_reset(&I.view, 96, 96, 0, 0, 48); view_plane(&I.view, I.which = 1);
  } else if (key == GLUT_KEY_PAGE_UP)   { view_zoom(&I.view,  0.1);
  } else if (key == GLUT_KEY_PAGE_DOWN) { view_zoom(&I.view, -0.1);
  } else                                { return; }
  render_do(&S.render, &I.view);
}

void interact_reshape(int w, int h) {
  view_reshape(&I.view, w, h);
}

void interact_begin() {
  I.which = 1;
  view_init(&I.view, 1024, 576, 96, 96, 0, 0, 48, 0);
  glutMouseFunc(interact_mouse);
  glutPassiveMotionFunc(interact_motion);
  glutKeyboardFunc(interact_keyboard);
  glutSpecialFunc(interact_special);
}
