#ifndef RENDER_H
#define RENDER_H 1

#include "view.h"

struct idlefunc_t { void (*f)(struct render *); };
struct donefunc_t { void (*f)(struct render *, void *); void *u; };

struct render {
  struct dvariable dvariable;
  struct prune prune;
  struct idlefunc_t idle;
  struct donefunc_t done;
  double samplerate;
  int reshape_requested;
  int win_width;
  int win_height;
  int tex_size;
  int vbo_size;
  GLuint tex;
  GLuint fbo;
  GLuint vbo[2];
  GLuint count;
  int steps;
  int total;
  struct view view;
  struct fillc fillc;
  struct init init;
  struct step step;
  struct plot plot;
  struct colour colour;
  struct timespec t_start;
  struct timespec t_update;
};

void render_begin(struct render *s, int d, float df);
void render_end(struct render *s);
void render_reshape(struct render *s, int w, int h);
void render_do(struct render *s, struct view *v);
void render_idle(struct render *s);
void render_display(struct render *s);
void render_ondone(struct render s, void (*f)(struct render *, void *), void *u);

#endif
