#!/bin/bash
echo "static const char $1[] ="
sed 's|\\|\\\\|g' |
sed 's|"|\\"|g' |
sed 's|^|"|' |
sed 's|$|\\n"|'
echo ";"
