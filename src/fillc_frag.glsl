smooth in vec2 t;
out layout(location = 0, index = 0) vec4 o;
void main() {
  o = vec4(t, 0.0, 0.0);
}
